<?php
/**
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'black_point_wp');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '123456');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}a:|gNW>Mfn5%!HFStW5AYEl<XSW>^Pjg|il|*|lkB<G5D3)@pY1(jTl9s(|I9-q');
define('SECURE_AUTH_KEY',  '@-[c*F8v4 _f1@3;4jTZuxrGSCBJ+AU/;xjKjek#_|x$R7:dcct)5_U%J{z4C`_C');
define('LOGGED_IN_KEY',    '*9XEV9}O}z cz,_6IJNaEmBT,_S qH6x%-cS}+q~(OaDQM~5cF|XRj?j<t{ w=>#');
define('NONCE_KEY',        'iy.#qW1R2D%G{qtQYNS;U|uDn(WVA+vT+6Rjc2DjpIE:sq~=.-H^&dcW695iM}1h');
define('AUTH_SALT',        '>x_,-2Is;P4vt;R:-N.,(sA,nz)(<>5==v k~[^SURkyD;a|E]CUaDT&)wT_0dzN');
define('SECURE_AUTH_SALT', '( lbZN7f1AR_u~}r>zxBbE0]G/1h]`iX-kfKLpDaR<w8&l!|Zqy3]wI-2oGX(yGI');
define('LOGGED_IN_SALT',   'h-G!*pWD3M*C:w+v.)#g),kJ2[uk?ro<a/S|LMV4v?jsQ{uJIj|;w_)r?=@T!nXf');
define('NONCE_SALT',       's/lq_MVt<S5kx}+r1~7k-e@5h|28vC}P8PfILmT&T[C8LM(:XDT~=[.@9Z&}^sDF');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
