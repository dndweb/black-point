-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 26-Jan-2015 às 19:19
-- Versão do servidor: 5.5.41-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `black_point_wp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_commentmeta`
--

CREATE TABLE IF NOT EXISTS `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `wp_commentmeta`
--

INSERT INTO `wp_commentmeta` (`meta_id`, `comment_id`, `meta_key`, `meta_value`) VALUES
(1, 2, 'is_customer_note', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_comments`
--

CREATE TABLE IF NOT EXISTS `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Sr. WordPress', '', 'https://wordpress.org/', '', '2015-01-09 17:51:15', '2015-01-09 17:51:15', 'Olá, Isto é um comentário.\nPara excluir um comentário, faça o login e veja os comentários de posts. Lá você terá a opção de editá-los ou excluí-los.', 0, '1', '', '', 0, 0),
(2, 129, 'WooCommerce', '', '', '', '2015-01-26 17:29:54', '2015-01-26 17:29:54', 'Pedido cancelado por falta de pagamento - tempo limite ultrapassado. Status do pedido alterado de pendente para cancelado.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_links`
--

CREATE TABLE IF NOT EXISTS `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_options`
--

CREATE TABLE IF NOT EXISTS `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=825 ;

--
-- Extraindo dados da tabela `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/blackpoint/', 'yes'),
(2, 'home', 'http://localhost/blackpoint/', 'yes'),
(3, 'blogname', 'Prosup', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'jeffersson.araujo@dndtec.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'gzipcompression', '0', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:6:{i:0;s:27:"acf-gallery/acf-gallery.php";i:1;s:39:"add-logo-to-admin/add-logo-to-admin.php";i:2;s:30:"advanced-custom-fields/acf.php";i:3;s:25:"qtranslate/qtranslate.php";i:4;s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";i:5;s:27:"woocommerce/woocommerce.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'advanced_edit', '0', 'yes'),
(37, 'comment_max_links', '2', 'yes'),
(38, 'gmt_offset', '0', 'yes'),
(39, 'default_email_category', '1', 'yes'),
(40, 'recently_edited', 'a:2:{i:0;s:106:"/home/jeffhammer/Documentos/WebDeveloper/Trabalhos/webSites/blackPoint/wp-content/themes/mystyle/style.css";i:2;s:0:"";}', 'no'),
(41, 'template', 'mystyle', 'yes'),
(42, 'stylesheet', 'mystyle', 'yes'),
(43, 'comment_whitelist', '1', 'yes'),
(44, 'blacklist_keys', '', 'no'),
(45, 'comment_registration', '0', 'yes'),
(46, 'html_type', 'text/html', 'yes'),
(47, 'use_trackback', '0', 'yes'),
(48, 'default_role', 'subscriber', 'yes'),
(49, 'db_version', '26691', 'yes'),
(50, 'uploads_use_yearmonth_folders', '1', 'yes'),
(51, 'upload_path', '', 'yes'),
(52, 'blog_public', '1', 'yes'),
(53, 'default_link_category', '2', 'yes'),
(54, 'show_on_front', 'posts', 'yes'),
(55, 'tag_base', '', 'yes'),
(56, 'show_avatars', '1', 'yes'),
(57, 'avatar_rating', 'G', 'yes'),
(58, 'upload_url_path', '', 'yes'),
(59, 'thumbnail_size_w', '150', 'yes'),
(60, 'thumbnail_size_h', '150', 'yes'),
(61, 'thumbnail_crop', '1', 'yes'),
(62, 'medium_size_w', '300', 'yes'),
(63, 'medium_size_h', '300', 'yes'),
(64, 'avatar_default', 'mystery', 'yes'),
(65, 'large_size_w', '1024', 'yes'),
(66, 'large_size_h', '1024', 'yes'),
(67, 'image_default_link_type', 'file', 'yes'),
(68, 'image_default_size', '', 'yes'),
(69, 'image_default_align', '', 'yes'),
(70, 'close_comments_for_old_posts', '0', 'yes'),
(71, 'close_comments_days_old', '14', 'yes'),
(72, 'thread_comments', '1', 'yes'),
(73, 'thread_comments_depth', '5', 'yes'),
(74, 'page_comments', '0', 'yes'),
(75, 'comments_per_page', '50', 'yes'),
(76, 'default_comments_page', 'newest', 'yes'),
(77, 'comment_order', 'asc', 'yes'),
(78, 'sticky_posts', 'a:0:{}', 'yes'),
(79, 'widget_categories', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_text', 'a:0:{}', 'yes'),
(81, 'widget_rss', 'a:0:{}', 'yes'),
(82, 'uninstall_plugins', 'a:1:{s:35:"qtranslate-slug/qtranslate-slug.php";s:13:"qts_uninstall";}', 'no'),
(83, 'timezone_string', '', 'yes'),
(84, 'page_for_posts', '0', 'yes'),
(85, 'page_on_front', '0', 'yes'),
(86, 'default_post_format', '0', 'yes'),
(87, 'link_manager_enabled', '0', 'yes'),
(88, 'initial_db_version', '29630', 'yes'),
(89, 'wp_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:132:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:9:"add_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:8:"customer";a:2:{s:4:"name";s:8:"Customer";s:12:"capabilities";a:3:{s:4:"read";b:1;s:10:"edit_posts";b:0;s:12:"delete_posts";b:0;}}s:12:"shop_manager";a:2:{s:4:"name";s:12:"Shop Manager";s:12:"capabilities";a:110:{s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:4:"read";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_users";b:1;s:10:"edit_posts";b:1;s:10:"edit_pages";b:1;s:20:"edit_published_posts";b:1;s:20:"edit_published_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:17:"edit_others_posts";b:1;s:17:"edit_others_pages";b:1;s:13:"publish_posts";b:1;s:13:"publish_pages";b:1;s:12:"delete_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:19:"delete_others_posts";b:1;s:19:"delete_others_pages";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:17:"moderate_comments";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:6:"export";b:1;s:6:"import";b:1;s:10:"list_users";b:1;s:18:"manage_woocommerce";b:1;s:24:"view_woocommerce_reports";b:1;s:12:"edit_product";b:1;s:12:"read_product";b:1;s:14:"delete_product";b:1;s:13:"edit_products";b:1;s:20:"edit_others_products";b:1;s:16:"publish_products";b:1;s:21:"read_private_products";b:1;s:15:"delete_products";b:1;s:23:"delete_private_products";b:1;s:25:"delete_published_products";b:1;s:22:"delete_others_products";b:1;s:21:"edit_private_products";b:1;s:23:"edit_published_products";b:1;s:20:"manage_product_terms";b:1;s:18:"edit_product_terms";b:1;s:20:"delete_product_terms";b:1;s:20:"assign_product_terms";b:1;s:15:"edit_shop_order";b:1;s:15:"read_shop_order";b:1;s:17:"delete_shop_order";b:1;s:16:"edit_shop_orders";b:1;s:23:"edit_others_shop_orders";b:1;s:19:"publish_shop_orders";b:1;s:24:"read_private_shop_orders";b:1;s:18:"delete_shop_orders";b:1;s:26:"delete_private_shop_orders";b:1;s:28:"delete_published_shop_orders";b:1;s:25:"delete_others_shop_orders";b:1;s:24:"edit_private_shop_orders";b:1;s:26:"edit_published_shop_orders";b:1;s:23:"manage_shop_order_terms";b:1;s:21:"edit_shop_order_terms";b:1;s:23:"delete_shop_order_terms";b:1;s:23:"assign_shop_order_terms";b:1;s:16:"edit_shop_coupon";b:1;s:16:"read_shop_coupon";b:1;s:18:"delete_shop_coupon";b:1;s:17:"edit_shop_coupons";b:1;s:24:"edit_others_shop_coupons";b:1;s:20:"publish_shop_coupons";b:1;s:25:"read_private_shop_coupons";b:1;s:19:"delete_shop_coupons";b:1;s:27:"delete_private_shop_coupons";b:1;s:29:"delete_published_shop_coupons";b:1;s:26:"delete_others_shop_coupons";b:1;s:25:"edit_private_shop_coupons";b:1;s:27:"edit_published_shop_coupons";b:1;s:24:"manage_shop_coupon_terms";b:1;s:22:"edit_shop_coupon_terms";b:1;s:24:"delete_shop_coupon_terms";b:1;s:24:"assign_shop_coupon_terms";b:1;s:17:"edit_shop_webhook";b:1;s:17:"read_shop_webhook";b:1;s:19:"delete_shop_webhook";b:1;s:18:"edit_shop_webhooks";b:1;s:25:"edit_others_shop_webhooks";b:1;s:21:"publish_shop_webhooks";b:1;s:26:"read_private_shop_webhooks";b:1;s:20:"delete_shop_webhooks";b:1;s:28:"delete_private_shop_webhooks";b:1;s:30:"delete_published_shop_webhooks";b:1;s:27:"delete_others_shop_webhooks";b:1;s:26:"edit_private_shop_webhooks";b:1;s:28:"edit_published_shop_webhooks";b:1;s:25:"manage_shop_webhook_terms";b:1;s:23:"edit_shop_webhook_terms";b:1;s:25:"delete_shop_webhook_terms";b:1;s:25:"assign_shop_webhook_terms";b:1;}}}', 'yes'),
(90, 'WPLANG', 'pt_BR', 'yes'),
(91, 'widget_search', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(92, 'widget_recent-posts', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(93, 'widget_recent-comments', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(94, 'widget_archives', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_meta', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:0:{}s:9:"sidebar-2";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(97, 'cron', 'a:9:{i:1422307055;a:1:{s:12:"qs_cron_hook";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1422307693;a:1:{s:32:"woocommerce_cancel_unpaid_orders";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:2:{s:8:"schedule";b:0;s:4:"args";a:0:{}}}}i:1422316800;a:1:{s:27:"woocommerce_scheduled_sales";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1422337887;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1422343948;a:1:{s:28:"woocommerce_cleanup_sessions";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1422344940;a:1:{s:20:"wp_maybe_auto_update";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1422381099;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1422383031;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(100, '_transient_random_seed', '23d43a8f0cd42af2f897b93d5ede95a5', 'yes'),
(108, 'auto_core_update_notified', 'a:4:{s:4:"type";s:6:"manual";s:5:"email";s:31:"jeffersson.araujo@dndtec.com.br";s:7:"version";s:5:"4.0.1";s:9:"timestamp";i:1420825893;}', 'yes'),
(131, '_transient_twentyfourteen_category_count', '1', 'yes'),
(135, 'current_theme', 'Twenty Thirteen/mystyle', 'yes'),
(136, 'theme_mods_mystyle', 'a:1:{i:0;b:0;}', 'yes'),
(137, 'theme_switched', '', 'yes'),
(139, 'theme_mods_twentyfourteen', 'a:1:{s:16:"sidebars_widgets";a:2:{s:4:"time";i:1420826108;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(140, 'theme_mods_twentythirteen', 'a:2:{i:0;b:0;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1420826209;s:4:"data";a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}}}}', 'yes'),
(168, 'recently_activated', 'a:2:{s:32:"global-translator/translator.php";i:1421867023;s:32:"setmore-appointments/setmore.php";i:1421862603;}', 'yes'),
(169, 'acf_version', '4.3.8', 'yes'),
(289, 'qtranslate_next_update_mo', '1422556024', 'yes'),
(292, 'qtranslate_language_names', 'a:17:{s:2:"de";s:7:"Deutsch";s:2:"en";s:7:"English";s:2:"zh";s:6:"中文";s:2:"fi";s:5:"suomi";s:2:"fr";s:9:"Français";s:2:"nl";s:10:"Nederlands";s:2:"sv";s:7:"Svenska";s:2:"it";s:8:"Italiano";s:2:"ro";s:8:"Română";s:2:"hu";s:6:"Magyar";s:2:"ja";s:9:"日本語";s:2:"es";s:8:"Español";s:2:"vi";s:14:"Tiếng Việt";s:2:"ar";s:14:"العربية";s:2:"pl";s:6:"Polski";s:2:"gl";s:6:"galego";s:2:"pt";s:10:"Português";}', 'yes'),
(293, 'qtranslate_enabled_languages', 'a:3:{i:0;s:2:"pt";i:1;s:2:"en";i:2;s:2:"es";}', 'yes'),
(294, 'qtranslate_default_language', 'pt', 'yes'),
(295, 'qtranslate_flag_location', 'plugins/qtranslate/flags/', 'yes'),
(296, 'qtranslate_flags', 'a:16:{s:2:"en";s:6:"gb.png";s:2:"de";s:6:"de.png";s:2:"zh";s:6:"cn.png";s:2:"fi";s:6:"fi.png";s:2:"fr";s:6:"fr.png";s:2:"nl";s:6:"nl.png";s:2:"sv";s:6:"se.png";s:2:"it";s:6:"it.png";s:2:"ro";s:6:"ro.png";s:2:"hu";s:6:"hu.png";s:2:"ja";s:6:"jp.png";s:2:"es";s:6:"es.png";s:2:"vi";s:6:"vn.png";s:2:"ar";s:8:"arle.png";s:2:"gl";s:10:"galego.png";s:2:"pt";s:6:"br.png";}', 'yes'),
(297, 'qtranslate_locales', 'a:17:{s:2:"de";s:5:"de_DE";s:2:"en";s:5:"en_US";s:2:"zh";s:5:"zh_CN";s:2:"fi";s:2:"fi";s:2:"fr";s:5:"fr_FR";s:2:"nl";s:5:"nl_NL";s:2:"sv";s:5:"sv_SE";s:2:"it";s:5:"it_IT";s:2:"ro";s:5:"ro_RO";s:2:"hu";s:5:"hu_HU";s:2:"ja";s:2:"ja";s:2:"es";s:5:"es_ES";s:2:"vi";s:2:"vi";s:2:"ar";s:2:"ar";s:2:"pl";s:5:"pl_PL";s:2:"gl";s:5:"gl_ES";s:2:"pt";s:5:"pt_BR";}', 'yes'),
(298, 'qtranslate_na_messages', 'a:17:{s:2:"de";s:58:"Leider ist der Eintrag nur auf %LANG:, : und % verfügbar.";s:2:"en";s:55:"Sorry, this entry is only available in %LANG:, : and %.";s:2:"zh";s:50:"对不起，此内容只适用于%LANG:，:和%。";s:2:"fi";s:92:"Anteeksi, mutta tämä kirjoitus on saatavana ainoastaan näillä kielillä: %LANG:, : ja %.";s:2:"fr";s:65:"Désolé, cet article est seulement disponible en %LANG:, : et %.";s:2:"nl";s:78:"Onze verontschuldigingen, dit bericht is alleen beschikbaar in %LANG:, : en %.";s:2:"sv";s:66:"Tyvärr är denna artikel enbart tillgänglig på %LANG:, : och %.";s:2:"it";s:71:"Ci spiace, ma questo articolo è disponibile soltanto in %LANG:, : e %.";s:2:"ro";s:67:"Din păcate acest articol este disponibil doar în %LANG:, : și %.";s:2:"hu";s:73:"Sajnos ennek a bejegyzésnek csak %LANG:, : és % nyelvű változata van.";s:2:"ja";s:97:"申し訳ありません、このコンテンツはただ今　%LANG:、 :と %　のみです。";s:2:"es";s:68:"Disculpa, pero esta entrada está disponible sólo en %LANG:, : y %.";s:2:"vi";s:63:"Rất tiếc, mục này chỉ tồn tại ở %LANG:, : và %.";s:2:"ar";s:73:"عفوا، هذه المدخلة موجودة فقط في %LANG:, : و %.";s:2:"pl";s:68:"Przepraszamy, ten wpis jest dostępny tylko w języku %LANG:, : i %.";s:2:"gl";s:66:"Sentímolo moito, ista entrada atopase unicamente en %LANG;,: e %.";s:2:"pt";s:70:"Desculpe-nos, mas este texto esta apenas disponível em %LANG:, : y %.";}', 'yes'),
(299, 'qtranslate_date_formats', 'a:17:{s:2:"en";s:14:"%A %B %e%q, %Y";s:2:"de";s:17:"%A, der %e. %B %Y";s:2:"zh";s:5:"%x %A";s:2:"fi";s:8:"%e.&m.%C";s:2:"fr";s:11:"%A %e %B %Y";s:2:"nl";s:8:"%d/%m/%y";s:2:"sv";s:8:"%Y/%m/%d";s:2:"it";s:8:"%e %B %Y";s:2:"ro";s:12:"%A, %e %B %Y";s:2:"hu";s:12:"%Y %B %e, %A";s:2:"ja";s:15:"%Y年%m月%d日";s:2:"es";s:14:"%d de %B de %Y";s:2:"vi";s:8:"%d/%m/%Y";s:2:"ar";s:8:"%d/%m/%Y";s:2:"pl";s:8:"%d/%m/%y";s:2:"gl";s:14:"%d de %B de %Y";s:2:"pt";s:14:"%d de %B de %Y";}', 'yes'),
(300, 'qtranslate_time_formats', 'a:17:{s:2:"en";s:8:"%I:%M %p";s:2:"de";s:5:"%H:%M";s:2:"zh";s:7:"%I:%M%p";s:2:"fi";s:5:"%H:%M";s:2:"fr";s:5:"%H:%M";s:2:"nl";s:5:"%H:%M";s:2:"sv";s:5:"%H:%M";s:2:"it";s:5:"%H:%M";s:2:"ro";s:5:"%H:%M";s:2:"hu";s:5:"%H:%M";s:2:"ja";s:5:"%H:%M";s:2:"es";s:10:"%H:%M hrs.";s:2:"vi";s:5:"%H:%M";s:2:"ar";s:5:"%H:%M";s:2:"pl";s:5:"%H:%M";s:2:"gl";s:10:"%H:%M hrs.";s:2:"pt";s:10:"%H:%M hrs.";}', 'yes'),
(301, 'qtranslate_ignore_file_types', 'gif,jpg,jpeg,png,pdf,swf,tif,rar,zip,7z,mpg,divx,mpeg,avi,css,js', 'yes'),
(302, 'qtranslate_url_mode', '2', 'yes'),
(303, 'qtranslate_term_name', 'a:0:{}', 'yes'),
(304, 'qtranslate_use_strftime', '3', 'yes'),
(305, 'qtranslate_detect_browser_language', '1', 'yes'),
(306, 'qtranslate_hide_untranslated', '0', 'yes'),
(307, 'qtranslate_auto_update_mo', '1', 'yes'),
(308, 'qtranslate_hide_default_language', '1', 'yes'),
(309, 'qtranslate_qtranslate_services', '0', 'yes'),
(311, 'db_upgraded', '', 'yes'),
(315, 'widget_qtranslate', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(359, 'links_recently_updated_prepend', '<em>', 'yes'),
(360, 'links_recently_updated_append', '</em>', 'yes'),
(361, 'links_recently_updated_time', '120', 'yes'),
(364, 'can_compress_scripts', '0', 'yes'),
(381, '_site_transient_timeout_browser_ee2e29b5e18302adfac38de9e1b7b3ae', '1422465078', 'yes'),
(382, '_site_transient_browser_ee2e29b5e18302adfac38de9e1b7b3ae', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"39.0.2171.95";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(395, '_transient_timeout_plugin_slugs', '1422047857', 'no'),
(396, '_transient_plugin_slugs', 'a:6:{i:0;s:39:"add-logo-to-admin/add-logo-to-admin.php";i:1;s:30:"advanced-custom-fields/acf.php";i:2;s:27:"acf-gallery/acf-gallery.php";i:3;s:25:"qtranslate/qtranslate.php";i:4;s:27:"woocommerce/woocommerce.php";i:5;s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";}', 'no'),
(420, 'gltr_base_lang', 'pt', 'yes'),
(421, 'gltr_col_num', '48', 'yes'),
(422, 'gltr_html_bar_tag', 'MAP', 'yes'),
(423, 'gltr_my_translation_engine', 'google', 'yes'),
(424, 'gltr_preferred_languages', 'a:2:{i:4;s:2:"en";i:7;s:2:"es";}', 'yes'),
(425, 'gltr_ban_prevention', '1', 'yes'),
(426, 'gltr_enable_debug', '', 'yes'),
(427, 'gltr_conn_interval', '300', 'yes'),
(428, 'gltr_sitemap_integration', '', 'yes'),
(429, 'gltr_last_connection_time', '1421866947', 'yes'),
(430, 'gltr_translation_status', 'unknown', 'yes'),
(431, 'gltr_cache_expire_time', '15', 'yes'),
(432, 'gltr_use_302', '', 'yes'),
(433, 'gltr_compress_cache', '', 'yes'),
(446, 'wp_add_logo_to_admin', 'a:3:{s:5:"login";s:2:"on";s:5:"image";s:80:"http://localhost/blackpoint/wp-content/plugins/add-logo-to-admin/images/logo.png";s:5:"admin";s:0:"";}', 'yes'),
(455, '_site_transient_timeout_browser_f5294302f2e7e4a15627fd577c84007f', '1422556040', 'yes'),
(456, '_site_transient_browser_f5294302f2e7e4a15627fd577c84007f', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"39.0.2171.99";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(471, 'woocommerce_default_country', 'BR:RN', 'yes'),
(472, 'woocommerce_allowed_countries', 'specific', 'yes'),
(473, 'woocommerce_specific_allowed_countries', 'a:1:{i:0;s:2:"BR";}', 'yes'),
(474, 'woocommerce_demo_store', 'no', 'yes'),
(475, 'woocommerce_demo_store_notice', 'This is a demo store for testing purposes — no orders shall be fulfilled.', 'no'),
(476, 'woocommerce_api_enabled', 'yes', 'yes'),
(477, 'woocommerce_currency', 'BRL', 'yes'),
(478, 'woocommerce_currency_pos', 'left', 'yes'),
(479, 'woocommerce_price_thousand_sep', ',', 'yes'),
(480, 'woocommerce_price_decimal_sep', '.', 'yes'),
(481, 'woocommerce_price_num_decimals', '2', 'yes'),
(482, 'woocommerce_enable_lightbox', 'yes', 'yes'),
(483, 'woocommerce_enable_chosen', 'yes', 'no'),
(484, 'woocommerce_shop_page_id', '119', 'yes'),
(485, 'woocommerce_shop_page_display', '', 'yes'),
(486, 'woocommerce_category_archive_display', '', 'yes'),
(487, 'woocommerce_default_catalog_orderby', 'menu_order', 'yes'),
(488, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(489, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(490, 'woocommerce_weight_unit', 'kg', 'yes'),
(491, 'woocommerce_dimension_unit', 'cm', 'yes'),
(492, 'woocommerce_enable_review_rating', 'no', 'no'),
(493, 'woocommerce_review_rating_required', 'yes', 'no'),
(494, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(495, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(496, 'shop_catalog_image_size', 'a:3:{s:5:"width";s:3:"150";s:6:"height";s:3:"150";s:4:"crop";i:1;}', 'yes'),
(497, 'shop_single_image_size', 'a:3:{s:5:"width";s:3:"300";s:6:"height";s:3:"300";s:4:"crop";i:1;}', 'yes'),
(498, 'shop_thumbnail_image_size', 'a:3:{s:5:"width";s:2:"90";s:6:"height";s:2:"90";s:4:"crop";i:1;}', 'yes'),
(499, 'woocommerce_file_download_method', 'force', 'no'),
(500, 'woocommerce_downloads_require_login', 'no', 'no'),
(501, 'woocommerce_downloads_grant_access_after_payment', 'no', 'no'),
(502, 'woocommerce_manage_stock', 'yes', 'yes'),
(503, 'woocommerce_hold_stock_minutes', '60', 'no'),
(504, 'woocommerce_notify_low_stock', 'yes', 'no'),
(505, 'woocommerce_notify_no_stock', 'yes', 'no'),
(506, 'woocommerce_stock_email_recipient', 'jeffersson.araujo@dndtec.com.br', 'no'),
(507, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(508, 'woocommerce_notify_no_stock_amount', '0', 'no'),
(509, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(510, 'woocommerce_stock_format', '', 'yes'),
(511, 'woocommerce_calc_taxes', 'no', 'yes'),
(512, 'woocommerce_prices_include_tax', 'no', 'yes'),
(513, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(514, 'woocommerce_default_customer_address', 'base', 'yes'),
(515, 'woocommerce_shipping_tax_class', 'title', 'yes'),
(516, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(517, 'woocommerce_tax_classes', 'Reduced Rate\nZero Rate', 'yes'),
(518, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(519, 'woocommerce_price_display_suffix', '', 'yes'),
(520, 'woocommerce_tax_display_cart', 'excl', 'no'),
(521, 'woocommerce_tax_total_display', 'itemized', 'no'),
(522, 'woocommerce_enable_coupons', 'no', 'no'),
(523, 'woocommerce_enable_guest_checkout', 'no', 'no'),
(524, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(525, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(526, 'woocommerce_cart_page_id', '120', 'yes'),
(527, 'woocommerce_checkout_page_id', '121', 'yes'),
(528, 'woocommerce_terms_page_id', '', 'no'),
(529, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(530, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(531, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(532, 'woocommerce_calc_shipping', 'no', 'yes'),
(533, 'woocommerce_enable_shipping_calc', 'no', 'no'),
(534, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(535, 'woocommerce_shipping_method_format', '', 'no'),
(536, 'woocommerce_ship_to_destination', 'shipping', 'no'),
(537, 'woocommerce_ship_to_countries', '', 'yes'),
(538, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(539, 'woocommerce_myaccount_page_id', '122', 'yes'),
(540, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(541, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(542, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(543, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(544, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(545, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(546, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(547, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(548, 'woocommerce_registration_generate_username', 'yes', 'no'),
(549, 'woocommerce_registration_generate_password', 'no', 'no'),
(550, 'woocommerce_email_from_name', 'Prosup', 'no'),
(551, 'woocommerce_email_from_address', 'jeffersson.araujo@dndtec.com.br', 'no'),
(552, 'woocommerce_email_header_image', '', 'no'),
(553, 'woocommerce_email_footer_text', 'Prosup - Powered by WooCommerce', 'no'),
(554, 'woocommerce_email_base_color', '#557da1', 'no'),
(555, 'woocommerce_email_background_color', '#f5f5f5', 'no'),
(556, 'woocommerce_email_body_background_color', '#fdfdfd', 'no'),
(557, 'woocommerce_email_text_color', '#505050', 'no'),
(559, 'woocommerce_db_version', '2.1.8', 'yes'),
(560, 'woocommerce_version', '2.1.8', 'yes'),
(567, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(568, 'woocommerce_admin_notices', 'a:1:{i:0;s:14:"template_files";}', 'yes'),
(574, 'woocommerce_language_pack_version', 'a:2:{i:0;s:6:"2.2.10";i:1;s:5:"pt_BR";}', 'yes'),
(584, 'woocommerce_ship_to_billing', 'yes', 'no'),
(585, 'woocommerce_ship_to_billing_address_only', 'no', 'no'),
(592, '_transient_timeout_wc_upgrade_notice_2.1.8', '1422041568', 'no'),
(593, '_transient_wc_upgrade_notice_2.1.8', '<div class="wc_plugin_upgrade_notice">2.2 is a major update. Test extensions and your theme prior to updating, ensure extensions are up to date and 2.2 compatible, and ensure that you keep backups. Developers should catch up with <a href="http://develop.woothemes.com/">develop.woothemes.com</a> to see what has been happening in core.</div> ', 'no'),
(604, 'slurp_page_installed', '1', 'yes'),
(628, 'woocommerce_default_shipping_method', '', 'yes'),
(629, 'woocommerce_shipping_method_order', 'a:5:{s:9:"flat_rate";i:0;s:13:"free_shipping";i:1;s:22:"international_delivery";i:2;s:14:"local_delivery";i:3;s:12:"local_pickup";i:4;}', 'yes'),
(646, '_transient_timeout_dash_4077549d03da2e451c8b5f002294ff51', '1422077956', 'no'),
(647, '_transient_dash_4077549d03da2e451c8b5f002294ff51', '<div class="rss-widget"><p><strong>Erro de RSS</strong>: WP HTTP Error: Could not resolve host: wordpress.org</p></div><div class="rss-widget"><p><strong>Erro de RSS</strong>: WP HTTP Error: Could not resolve host: planet.wordpress.org</p></div><div class="rss-widget"><ul></ul></div>', 'no'),
(664, '_site_transient_timeout_browser_7f3aec599605a7bbe622da121287a416', '1422643011', 'yes'),
(665, '_site_transient_browser_7f3aec599605a7bbe622da121287a416', 'a:9:{s:8:"platform";s:5:"Linux";s:4:"name";s:6:"Chrome";s:7:"version";s:12:"40.0.2214.91";s:10:"update_url";s:28:"http://www.google.com/chrome";s:7:"img_src";s:49:"http://s.wordpress.org/images/browsers/chrome.png";s:11:"img_src_ssl";s:48:"https://wordpress.org/images/browsers/chrome.png";s:15:"current_version";s:2:"18";s:7:"upgrade";b:0;s:8:"insecure";b:0;}', 'yes'),
(684, 'woocommerce_permalinks', 'a:4:{s:13:"category_base";s:0:"";s:8:"tag_base";s:0:"";s:14:"attribute_base";s:0:"";s:12:"product_base";s:19:"/loja/%product_cat%";}', 'yes'),
(703, 'woocommerce_pagseguro_settings', 'a:17:{s:7:"enabled";s:3:"yes";s:5:"title";s:9:"PagSeguro";s:11:"description";s:19:"Pagar com PagSeguro";s:5:"email";s:20:"airasana@hotmail.com";s:5:"token";s:10:"iloveJESUS";s:6:"method";s:8:"redirect";s:20:"transparent_checkout";s:0:"";s:9:"tc_credit";s:3:"yes";s:11:"tc_transfer";s:3:"yes";s:9:"tc_ticket";s:3:"yes";s:17:"tc_ticket_message";s:3:"yes";s:8:"behavior";s:0:"";s:15:"send_only_total";s:2:"no";s:14:"invoice_prefix";s:3:"WC-";s:7:"testing";s:0:"";s:7:"sandbox";s:2:"no";s:5:"debug";s:2:"no";}', 'yes'),
(707, 'woocommerce_cheque_settings', 'a:4:{s:7:"enabled";s:2:"no";s:5:"title";s:20:"Pagamento por cheque";s:11:"description";s:112:"Por favor, envie seu cheque para Nome da Loja, Rua da Loja, Cidade da Loja, Estado / País da Loja, CEP da Loja.";s:12:"instructions";s:112:"Por favor, envie seu cheque para Nome da Loja, Rua da Loja, Cidade da Loja, Estado / País da Loja, CEP da Loja.";}', 'yes'),
(714, 'woocommerce_paypal_settings', 'a:16:{s:7:"enabled";s:2:"no";s:5:"title";s:6:"PayPal";s:11:"description";s:66:"Pagamento via PayPal. Você pode pagar com seu cartão de crédito";s:5:"email";s:0:"";s:14:"receiver_email";s:0:"";s:14:"identity_token";s:0:"";s:14:"invoice_prefix";s:3:"WC-";s:13:"paymentaction";s:4:"sale";s:22:"form_submission_method";s:2:"no";s:10:"page_style";s:0:"";s:8:"shipping";s:0:"";s:13:"send_shipping";s:2:"no";s:16:"address_override";s:2:"no";s:7:"testing";s:0:"";s:8:"testmode";s:2:"no";s:5:"debug";s:2:"no";}', 'yes'),
(718, 'woocommerce_bacs_settings', 'a:5:{s:7:"enabled";s:2:"no";s:5:"title";s:31:"Transferência bancária direta";s:11:"description";s:262:"Faça seu pagamento diretamente em nossa conta bancária. Se possível, por favor informe o ID do seu pedido como identificação do seu depósito ou transferência. Para pagamentos via DOC, seu pedido não será enviado enquanto o pagamento não for compensado.";s:12:"instructions";s:262:"Faça seu pagamento diretamente em nossa conta bancária. Se possível, por favor informe o ID do seu pedido como identificação do seu depósito ou transferência. Para pagamentos via DOC, seu pedido não será enviado enquanto o pagamento não for compensado.";s:15:"account_details";s:0:"";}', 'yes'),
(719, 'woocommerce_bacs_accounts', 'a:1:{i:0;a:6:{s:12:"account_name";s:0:"";s:14:"account_number";s:0:"";s:9:"bank_name";s:0:"";s:9:"sort_code";s:0:"";s:4:"iban";s:0:"";s:3:"bic";s:0:"";}}', 'yes'),
(773, 'woocommerce_default_gateway', '', 'yes'),
(774, 'woocommerce_gateway_order', 'a:6:{s:4:"bacs";i:0;s:6:"cheque";i:1;s:3:"cod";i:2;s:16:"mijireh_checkout";i:3;s:6:"paypal";i:4;s:9:"pagseguro";i:5;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(775, 'rewrite_rules', 'a:230:{s:14:"^wc-api\\/v1/?$";s:24:"index.php?wc-api-route=/";s:16:"^wc-api\\/v1(.*)?";s:34:"index.php?wc-api-route=$matches[1]";s:7:"loja/?$";s:27:"index.php?post_type=product";s:37:"loja/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:32:"loja/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=product&feed=$matches[1]";s:24:"loja/page/([0-9]{1,})/?$";s:45:"index.php?post_type=product&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:32:"category/(.+?)/wc-api(/(.*))?/?$";s:54:"index.php?category_name=$matches[1]&wc-api=$matches[3]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:29:"tag/([^/]+)/wc-api(/(.*))?/?$";s:44:"index.php?tag=$matches[1]&wc-api=$matches[3]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:56:"categoria-produto/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:51:"categoria-produto/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:44:"categoria-produto/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:26:"categoria-produto/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:57:"produto-etiqueta/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:52:"produto-etiqueta/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_tag=$matches[1]&feed=$matches[2]";s:45:"produto-etiqueta/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?product_tag=$matches[1]&paged=$matches[2]";s:27:"produto-etiqueta/([^/]+)/?$";s:33:"index.php?product_tag=$matches[1]";s:36:"loja/.+?/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"loja/.+?/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"loja/.+?/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"loja/.+?/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"loja/.+?/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"loja/(.+?)/([^/]+)/trackback/?$";s:58:"index.php?product_cat=$matches[1]&product=$matches[2]&tb=1";s:51:"loja/(.+?)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:70:"index.php?product_cat=$matches[1]&product=$matches[2]&feed=$matches[3]";s:46:"loja/(.+?)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:70:"index.php?product_cat=$matches[1]&product=$matches[2]&feed=$matches[3]";s:39:"loja/(.+?)/([^/]+)/page/?([0-9]{1,})/?$";s:71:"index.php?product_cat=$matches[1]&product=$matches[2]&paged=$matches[3]";s:46:"loja/(.+?)/([^/]+)/comment-page-([0-9]{1,})/?$";s:71:"index.php?product_cat=$matches[1]&product=$matches[2]&cpage=$matches[3]";s:36:"loja/(.+?)/([^/]+)/wc-api(/(.*))?/?$";s:72:"index.php?product_cat=$matches[1]&product=$matches[2]&wc-api=$matches[4]";s:40:"loja/.+?/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:51:"loja/.+?/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:31:"loja/(.+?)/([^/]+)(/[0-9]+)?/?$";s:70:"index.php?product_cat=$matches[1]&product=$matches[2]&page=$matches[3]";s:25:"loja/.+?/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"loja/.+?/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"loja/.+?/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"loja/.+?/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"loja/.+?/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"loja/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:38:"loja/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?product_cat=$matches[1]&feed=$matches[2]";s:31:"loja/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&paged=$matches[2]";s:38:"loja/(.+?)/comment-page-([0-9]{1,})/?$";s:51:"index.php?product_cat=$matches[1]&cpage=$matches[2]";s:28:"loja/(.+?)/wc-api(/(.*))?/?$";s:52:"index.php?product_cat=$matches[1]&wc-api=$matches[3]";s:13:"loja/(.+?)/?$";s:33:"index.php?product_cat=$matches[1]";s:45:"product_variation/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:55:"product_variation/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:75:"product_variation/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:70:"product_variation/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:38:"product_variation/([^/]+)/trackback/?$";s:44:"index.php?product_variation=$matches[1]&tb=1";s:46:"product_variation/([^/]+)/page/?([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&paged=$matches[2]";s:53:"product_variation/([^/]+)/comment-page-([0-9]{1,})/?$";s:57:"index.php?product_variation=$matches[1]&cpage=$matches[2]";s:43:"product_variation/([^/]+)/wc-api(/(.*))?/?$";s:58:"index.php?product_variation=$matches[1]&wc-api=$matches[3]";s:49:"product_variation/[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:60:"product_variation/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:38:"product_variation/([^/]+)(/[0-9]+)?/?$";s:56:"index.php?product_variation=$matches[1]&page=$matches[2]";s:34:"product_variation/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"product_variation/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"product_variation/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"product_variation/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"galeria/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:43:"galeria/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:63:"galeria/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"galeria/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:58:"galeria/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:26:"galeria/(.+?)/trackback/?$";s:34:"index.php?galeria=$matches[1]&tb=1";s:34:"galeria/(.+?)/page/?([0-9]{1,})/?$";s:47:"index.php?galeria=$matches[1]&paged=$matches[2]";s:41:"galeria/(.+?)/comment-page-([0-9]{1,})/?$";s:47:"index.php?galeria=$matches[1]&cpage=$matches[2]";s:31:"galeria/(.+?)/wc-api(/(.*))?/?$";s:48:"index.php?galeria=$matches[1]&wc-api=$matches[3]";s:37:"galeria/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:48:"galeria/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:26:"galeria/(.+?)(/[0-9]+)?/?$";s:46:"index.php?galeria=$matches[1]&page=$matches[2]";s:31:"sobre/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:41:"sobre/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:61:"sobre/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"sobre/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:56:"sobre/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:24:"sobre/(.+?)/trackback/?$";s:32:"index.php?sobre=$matches[1]&tb=1";s:32:"sobre/(.+?)/page/?([0-9]{1,})/?$";s:45:"index.php?sobre=$matches[1]&paged=$matches[2]";s:39:"sobre/(.+?)/comment-page-([0-9]{1,})/?$";s:45:"index.php?sobre=$matches[1]&cpage=$matches[2]";s:29:"sobre/(.+?)/wc-api(/(.*))?/?$";s:46:"index.php?sobre=$matches[1]&wc-api=$matches[3]";s:35:"sobre/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:46:"sobre/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:24:"sobre/(.+?)(/[0-9]+)?/?$";s:44:"index.php?sobre=$matches[1]&page=$matches[2]";s:37:"diferencial/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"diferencial/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"diferencial/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"diferencial/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"diferencial/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:30:"diferencial/(.+?)/trackback/?$";s:38:"index.php?diferencial=$matches[1]&tb=1";s:38:"diferencial/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?diferencial=$matches[1]&paged=$matches[2]";s:45:"diferencial/(.+?)/comment-page-([0-9]{1,})/?$";s:51:"index.php?diferencial=$matches[1]&cpage=$matches[2]";s:35:"diferencial/(.+?)/wc-api(/(.*))?/?$";s:52:"index.php?diferencial=$matches[1]&wc-api=$matches[3]";s:41:"diferencial/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:52:"diferencial/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:30:"diferencial/(.+?)(/[0-9]+)?/?$";s:50:"index.php?diferencial=$matches[1]&page=$matches[2]";s:34:"servicos/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"servicos/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"servicos/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"servicos/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"servicos/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:27:"servicos/(.+?)/trackback/?$";s:35:"index.php?servicos=$matches[1]&tb=1";s:35:"servicos/(.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?servicos=$matches[1]&paged=$matches[2]";s:42:"servicos/(.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?servicos=$matches[1]&cpage=$matches[2]";s:32:"servicos/(.+?)/wc-api(/(.*))?/?$";s:49:"index.php?servicos=$matches[1]&wc-api=$matches[3]";s:38:"servicos/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:49:"servicos/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:27:"servicos/(.+?)(/[0-9]+)?/?$";s:47:"index.php?servicos=$matches[1]&page=$matches[2]";s:35:"aventuras/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:45:"aventuras/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:65:"aventuras/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"aventuras/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:60:"aventuras/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:28:"aventuras/(.+?)/trackback/?$";s:36:"index.php?aventuras=$matches[1]&tb=1";s:36:"aventuras/(.+?)/page/?([0-9]{1,})/?$";s:49:"index.php?aventuras=$matches[1]&paged=$matches[2]";s:43:"aventuras/(.+?)/comment-page-([0-9]{1,})/?$";s:49:"index.php?aventuras=$matches[1]&cpage=$matches[2]";s:33:"aventuras/(.+?)/wc-api(/(.*))?/?$";s:50:"index.php?aventuras=$matches[1]&wc-api=$matches[3]";s:39:"aventuras/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:50:"aventuras/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:28:"aventuras/(.+?)(/[0-9]+)?/?$";s:48:"index.php?aventuras=$matches[1]&page=$matches[2]";s:32:"slides/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:42:"slides/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:62:"slides/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"slides/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:57:"slides/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:25:"slides/(.+?)/trackback/?$";s:33:"index.php?slides=$matches[1]&tb=1";s:33:"slides/(.+?)/page/?([0-9]{1,})/?$";s:46:"index.php?slides=$matches[1]&paged=$matches[2]";s:40:"slides/(.+?)/comment-page-([0-9]{1,})/?$";s:46:"index.php?slides=$matches[1]&cpage=$matches[2]";s:30:"slides/(.+?)/wc-api(/(.*))?/?$";s:47:"index.php?slides=$matches[1]&wc-api=$matches[3]";s:36:"slides/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:47:"slides/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:25:"slides/(.+?)(/[0-9]+)?/?$";s:45:"index.php?slides=$matches[1]&page=$matches[2]";s:30:"menu/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:40:"menu/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:60:"menu/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"menu/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:55:"menu/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:23:"menu/(.+?)/trackback/?$";s:31:"index.php?menu=$matches[1]&tb=1";s:31:"menu/(.+?)/page/?([0-9]{1,})/?$";s:44:"index.php?menu=$matches[1]&paged=$matches[2]";s:38:"menu/(.+?)/comment-page-([0-9]{1,})/?$";s:44:"index.php?menu=$matches[1]&cpage=$matches[2]";s:28:"menu/(.+?)/wc-api(/(.*))?/?$";s:45:"index.php?menu=$matches[1]&wc-api=$matches[3]";s:34:"menu/.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:45:"menu/.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:23:"menu/(.+?)(/[0-9]+)?/?$";s:43:"index.php?menu=$matches[1]&page=$matches[2]";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:17:"wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:26:"comments/wc-api(/(.*))?/?$";s:29:"index.php?&wc-api=$matches[2]";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:29:"search/(.+)/wc-api(/(.*))?/?$";s:42:"index.php?s=$matches[1]&wc-api=$matches[3]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:32:"author/([^/]+)/wc-api(/(.*))?/?$";s:52:"index.php?author_name=$matches[1]&wc-api=$matches[3]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:54:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:82:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:41:"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$";s:66:"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:28:"([0-9]{4})/wc-api(/(.*))?/?$";s:45:"index.php?year=$matches[1]&wc-api=$matches[3]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:25:"(.?.+?)/wc-api(/(.*))?/?$";s:49:"index.php?pagename=$matches[1]&wc-api=$matches[3]";s:28:"(.?.+?)/order-pay(/(.*))?/?$";s:52:"index.php?pagename=$matches[1]&order-pay=$matches[3]";s:33:"(.?.+?)/order-received(/(.*))?/?$";s:57:"index.php?pagename=$matches[1]&order-received=$matches[3]";s:29:"(.?.+?)/view-order(/(.*))?/?$";s:53:"index.php?pagename=$matches[1]&view-order=$matches[3]";s:31:"(.?.+?)/edit-account(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-account=$matches[3]";s:31:"(.?.+?)/edit-address(/(.*))?/?$";s:55:"index.php?pagename=$matches[1]&edit-address=$matches[3]";s:32:"(.?.+?)/lost-password(/(.*))?/?$";s:56:"index.php?pagename=$matches[1]&lost-password=$matches[3]";s:34:"(.?.+?)/customer-logout(/(.*))?/?$";s:58:"index.php?pagename=$matches[1]&customer-logout=$matches[3]";s:37:"(.?.+?)/add-payment-method(/(.*))?/?$";s:61:"index.php?pagename=$matches[1]&add-payment-method=$matches[3]";s:31:".?.+?/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:20:"(.?.+?)(/[0-9]+)?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:25:"([^/]+)/wc-api(/(.*))?/?$";s:45:"index.php?name=$matches[1]&wc-api=$matches[3]";s:31:"[^/]+/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:42:"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$";s:51:"index.php?attachment=$matches[1]&wc-api=$matches[3]";s:20:"([^/]+)(/[0-9]+)?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";}', 'yes'),
(776, '_transient_woocommerce_cache_excluded_uris', 'a:6:{i:0;s:5:"p=120";i:1;s:5:"p=121";i:2;s:5:"p=122";i:3;s:9:"/carrinho";i:4;s:17:"/finalizar-compra";i:5;s:12:"/minha-conta";}', 'yes'),
(777, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(778, '_transient_timeout_wc_rating_count_128', '1453583578', 'no'),
(779, '_transient_wc_rating_count_128', '0', 'no'),
(780, '_transient_timeout_wc_average_rating_128', '1453583578', 'no'),
(781, '_transient_wc_average_rating_128', '', 'no'),
(782, '_transient_timeout_wc_rating_count_127', '1453583578', 'no'),
(783, '_transient_wc_rating_count_127', '0', 'no'),
(784, '_transient_timeout_wc_average_rating_127', '1453583578', 'no'),
(785, '_transient_wc_average_rating_127', '', 'no'),
(786, '_transient_timeout_wc_rating_count_126', '1453583578', 'no'),
(787, '_transient_wc_rating_count_126', '0', 'no'),
(788, '_transient_timeout_wc_average_rating_126', '1453583578', 'no'),
(789, '_transient_wc_average_rating_126', '', 'no'),
(790, '_transient_timeout_wc_rating_count_125', '1453583578', 'no'),
(791, '_transient_wc_rating_count_125', '0', 'no'),
(792, '_transient_timeout_wc_average_rating_125', '1453583578', 'no'),
(793, '_transient_wc_average_rating_125', '', 'no'),
(794, '_transient_timeout_wc_rating_count_124', '1453583578', 'no'),
(795, '_transient_wc_rating_count_124', '0', 'no'),
(796, '_transient_timeout_wc_average_rating_124', '1453583578', 'no'),
(797, '_transient_wc_average_rating_124', '', 'no'),
(807, '_transient_timeout_woocommerce_processing_order_count', '1453830113', 'no'),
(808, '_transient_woocommerce_processing_order_count', '0', 'no'),
(809, '_wc_session_1', 'a:18:{s:10:"wc_notices";N;s:4:"cart";s:242:"a:1:{s:32:"3def184ad8f4755ff269862ea77393dd";a:8:{s:10:"product_id";i:125;s:12:"variation_id";s:0:"";s:9:"variation";s:0:"";s:8:"quantity";i:1;s:10:"line_total";d:30;s:8:"line_tax";i:0;s:13:"line_subtotal";i:30;s:17:"line_subtotal_tax";i:0;}}";s:15:"applied_coupons";s:6:"a:0:{}";s:14:"shipping_total";N;s:23:"coupon_discount_amounts";s:6:"a:0:{}";s:19:"cart_contents_total";d:30;s:20:"cart_contents_weight";i:0;s:19:"cart_contents_count";i:1;s:17:"cart_contents_tax";i:0;s:5:"total";d:30;s:8:"subtotal";i:30;s:15:"subtotal_ex_tax";i:30;s:9:"tax_total";i:0;s:5:"taxes";s:6:"a:0:{}";s:14:"shipping_taxes";s:6:"a:0:{}";s:13:"discount_cart";i:0;s:14:"discount_total";i:0;s:18:"shipping_tax_total";i:0;}', 'no'),
(810, '_wc_session_expires_1', '1422466194', 'no'),
(813, '_transient_timeout_wc_uf_pid_98445f98e21fece4c092bccf3dc7ac25', '1453832549', 'no'),
(814, '_transient_wc_uf_pid_98445f98e21fece4c092bccf3dc7ac25', 'a:5:{i:0;s:3:"128";i:1;s:3:"127";i:2;s:3:"126";i:3;s:3:"125";i:4;s:3:"124";}', 'no'),
(818, '_site_transient_timeout_theme_roots', '1422305894', 'yes'),
(819, '_site_transient_theme_roots', 'a:4:{s:7:"mystyle";s:7:"/themes";s:14:"twentyfourteen";s:7:"/themes";s:14:"twentythirteen";s:7:"/themes";s:12:"twentytwelve";s:7:"/themes";}', 'yes'),
(822, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:5:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:57:"https://downloads.wordpress.org/release/wordpress-4.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:57:"https://downloads.wordpress.org/release/wordpress-4.1.zip";s:10:"no_content";s:68:"https://downloads.wordpress.org/release/wordpress-4.1-no-content.zip";s:11:"new_bundled";s:69:"https://downloads.wordpress.org/release/wordpress-4.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"4.1";s:7:"version";s:3:"4.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:1;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:57:"https://downloads.wordpress.org/release/wordpress-4.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:57:"https://downloads.wordpress.org/release/wordpress-4.1.zip";s:10:"no_content";s:68:"https://downloads.wordpress.org/release/wordpress-4.1-no-content.zip";s:11:"new_bundled";s:69:"https://downloads.wordpress.org/release/wordpress-4.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:3:"4.1";s:7:"version";s:3:"4.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:2;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.0.1.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.0.1.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.0.1-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.0.1-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.0.1";s:7:"version";s:5:"4.0.1";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";s:12:"notify_email";s:1:"1";}i:3;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-3.9.3.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-3.9.3.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-3.9.3-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-3.9.3-new-bundled.zip";s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"3.9.3";s:7:"version";s:5:"3.9.3";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:0:"";}i:4;O:8:"stdClass":10:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-3.8.5.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-3.8.5.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-3.8.5-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-3.8.5-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-3.8.5-partial-1.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-3.8.5-rollback-1.zip";}s:7:"current";s:5:"3.8.5";s:7:"version";s:5:"3.8.5";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.1";s:15:"partial_version";s:5:"3.8.1";}}s:12:"last_checked";i:1422304097;s:15:"version_checked";s:5:"3.8.1";s:12:"translations";a:0:{}}', 'yes'),
(823, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1422304098;s:7:"checked";a:4:{s:7:"mystyle";s:3:"1.2";s:14:"twentyfourteen";s:3:"1.2";s:14:"twentythirteen";s:3:"1.2";s:12:"twentytwelve";s:3:"1.5";}s:8:"response";a:3:{s:14:"twentyfourteen";a:4:{s:5:"theme";s:14:"twentyfourteen";s:11:"new_version";s:3:"1.3";s:3:"url";s:43:"https://wordpress.org/themes/twentyfourteen";s:7:"package";s:60:"https://downloads.wordpress.org/theme/twentyfourteen.1.3.zip";}s:14:"twentythirteen";a:4:{s:5:"theme";s:14:"twentythirteen";s:11:"new_version";s:3:"1.4";s:3:"url";s:43:"https://wordpress.org/themes/twentythirteen";s:7:"package";s:60:"https://downloads.wordpress.org/theme/twentythirteen.1.4.zip";}s:12:"twentytwelve";a:4:{s:5:"theme";s:12:"twentytwelve";s:11:"new_version";s:3:"1.6";s:3:"url";s:41:"https://wordpress.org/themes/twentytwelve";s:7:"package";s:58:"https://downloads.wordpress.org/theme/twentytwelve.1.6.zip";}}s:12:"translations";a:1:{i:0;a:7:{s:4:"type";s:5:"theme";s:4:"slug";s:14:"twentyfourteen";s:8:"language";s:5:"pt_BR";s:7:"version";s:3:"1.2";s:7:"updated";s:19:"2014-12-18 13:13:21";s:7:"package";s:78:"https://downloads.wordpress.org/translation/theme/twentyfourteen/1.2/pt_BR.zip";s:10:"autoupdate";b:1;}}}', 'yes'),
(824, '_site_transient_update_plugins', 'O:8:"stdClass":3:{s:12:"last_checked";i:1422304098;s:8:"response";a:3:{s:30:"advanced-custom-fields/acf.php";O:8:"stdClass":6:{s:2:"id";s:5:"21367";s:4:"slug";s:22:"advanced-custom-fields";s:6:"plugin";s:30:"advanced-custom-fields/acf.php";s:11:"new_version";s:5:"4.3.9";s:3:"url";s:53:"https://wordpress.org/plugins/advanced-custom-fields/";s:7:"package";s:65:"https://downloads.wordpress.org/plugin/advanced-custom-fields.zip";}s:27:"woocommerce/woocommerce.php";O:8:"stdClass":6:{s:2:"id";s:5:"25331";s:4:"slug";s:11:"woocommerce";s:6:"plugin";s:27:"woocommerce/woocommerce.php";s:11:"new_version";s:6:"2.2.10";s:3:"url";s:42:"https://wordpress.org/plugins/woocommerce/";s:7:"package";s:61:"https://downloads.wordpress.org/plugin/woocommerce.2.2.10.zip";}s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";O:8:"stdClass":6:{s:2:"id";s:5:"35013";s:4:"slug";s:21:"woocommerce-pagseguro";s:6:"plugin";s:47:"woocommerce-pagseguro/woocommerce-pagseguro.php";s:11:"new_version";s:5:"2.7.4";s:3:"url";s:52:"https://wordpress.org/plugins/woocommerce-pagseguro/";s:7:"package";s:70:"https://downloads.wordpress.org/plugin/woocommerce-pagseguro.2.7.4.zip";}}s:12:"translations";a:0:{}}', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_postmeta`
--

CREATE TABLE IF NOT EXISTS `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=534 ;

--
-- Extraindo dados da tabela `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1420827699:1'),
(4, 6, '_edit_last', '1'),
(5, 6, '_edit_lock', '1421346305:1'),
(6, 8, '_edit_last', '1'),
(7, 8, '_edit_lock', '1420834973:1'),
(8, 10, '_edit_last', '1'),
(9, 10, '_edit_lock', '1421268886:1'),
(10, 12, '_edit_last', '1'),
(11, 12, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:7:"galeria";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(12, 12, 'position', 'normal'),
(13, 12, 'layout', 'no_box'),
(14, 12, 'hide_on_screen', 'a:13:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:10:"categories";i:11;s:4:"tags";i:12;s:15:"send-trackbacks";}'),
(15, 12, '_edit_lock', '1421081889:1'),
(16, 13, '_edit_last', '1'),
(17, 13, '_edit_lock', '1421081953:1'),
(18, 14, '_wp_attached_file', '2015/01/995092_10203769547772628_2306328007135427791_n.jpg'),
(19, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:600;s:4:"file";s:58:"2015/01/995092_10203769547772628_2306328007135427791_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"995092_10203769547772628_2306328007135427791_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"995092_10203769547772628_2306328007135427791_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"995092_10203769547772628_2306328007135427791_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(20, 15, '_wp_attached_file', '2015/01/10013517_274328936080420_7280410948369485414_n.jpg'),
(21, 15, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:58:"2015/01/10013517_274328936080420_7280410948369485414_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"10013517_274328936080420_7280410948369485414_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"10013517_274328936080420_7280410948369485414_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"10013517_274328936080420_7280410948369485414_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(22, 16, '_wp_attached_file', '2015/01/10013517_274328936080420_7280410948369485414_n-1.jpg'),
(23, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:60:"2015/01/10013517_274328936080420_7280410948369485414_n-1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10013517_274328936080420_7280410948369485414_n-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10013517_274328936080420_7280410948369485414_n-1-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10013517_274328936080420_7280410948369485414_n-1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(24, 17, '_wp_attached_file', '2015/01/10264786_275492659297381_9156587289541906779_n.jpg'),
(25, 17, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:709;s:4:"file";s:58:"2015/01/10264786_275492659297381_9156587289541906779_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"10264786_275492659297381_9156587289541906779_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"10264786_275492659297381_9156587289541906779_n-300x221.jpg";s:5:"width";i:300;s:6:"height";i:221;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"10264786_275492659297381_9156587289541906779_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(26, 18, '_wp_attached_file', '2015/01/10277419_275428175970496_1040750854537516087_n.jpg'),
(27, 18, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:58:"2015/01/10277419_275428175970496_1040750854537516087_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"10277419_275428175970496_1040750854537516087_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"10277419_275428175970496_1040750854537516087_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"10277419_275428175970496_1040750854537516087_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(28, 19, '_wp_attached_file', '2015/01/10306248_274151272764853_372873709152478271_n.jpg'),
(29, 19, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:57:"2015/01/10306248_274151272764853_372873709152478271_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:57:"10306248_274151272764853_372873709152478271_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:57:"10306248_274151272764853_372873709152478271_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:57:"10306248_274151272764853_372873709152478271_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(30, 20, '_wp_attached_file', '2015/01/10348651_274892456024068_5587817048245044284_n.jpg'),
(31, 20, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:58:"2015/01/10348651_274892456024068_5587817048245044284_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"10348651_274892456024068_5587817048245044284_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"10348651_274892456024068_5587817048245044284_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"10348651_274892456024068_5587817048245044284_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(32, 21, '_wp_attached_file', '2015/01/10402546_277417949104852_2759697881386835204_n.jpg'),
(33, 21, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:723;s:4:"file";s:58:"2015/01/10402546_277417949104852_2759697881386835204_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:58:"10402546_277417949104852_2759697881386835204_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:58:"10402546_277417949104852_2759697881386835204_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:58:"10402546_277417949104852_2759697881386835204_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(34, 22, '_wp_attached_file', '2015/01/10525997_10203382601699218_2848369748332605124_n.jpg'),
(35, 22, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:640;s:4:"file";s:60:"2015/01/10525997_10203382601699218_2848369748332605124_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10525997_10203382601699218_2848369748332605124_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10525997_10203382601699218_2848369748332605124_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10525997_10203382601699218_2848369748332605124_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(36, 23, '_wp_attached_file', '2015/01/10600661_10203769418729402_7009142274452723985_n.jpg'),
(37, 23, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:600;s:4:"file";s:60:"2015/01/10600661_10203769418729402_7009142274452723985_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10600661_10203769418729402_7009142274452723985_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10600661_10203769418729402_7009142274452723985_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10600661_10203769418729402_7009142274452723985_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(38, 24, '_wp_attached_file', '2015/01/10639605_10203769425409569_4100331693343799608_n.jpg'),
(39, 24, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:800;s:4:"file";s:60:"2015/01/10639605_10203769425409569_4100331693343799608_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10639605_10203769425409569_4100331693343799608_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10639605_10203769425409569_4100331693343799608_n-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10639605_10203769425409569_4100331693343799608_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(40, 25, '_wp_attached_file', '2015/01/10672270_10203769419329417_3629846643131503150_n.jpg'),
(41, 25, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:600;s:4:"file";s:60:"2015/01/10672270_10203769419329417_3629846643131503150_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10672270_10203769419329417_3629846643131503150_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10672270_10203769419329417_3629846643131503150_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10672270_10203769419329417_3629846643131503150_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(42, 26, '_wp_attached_file', '2015/01/10676362_10203769545812579_851785640718358102_n.jpg'),
(43, 26, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:800;s:6:"height";i:534;s:4:"file";s:59:"2015/01/10676362_10203769545812579_851785640718358102_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:59:"10676362_10203769545812579_851785640718358102_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:59:"10676362_10203769545812579_851785640718358102_n-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:59:"10676362_10203769545812579_851785640718358102_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(44, 27, '_wp_attached_file', '2015/01/10685466_10203769410449195_2417081619697543338_n.jpg'),
(45, 27, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:60:"2015/01/10685466_10203769410449195_2417081619697543338_n.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:60:"10685466_10203769410449195_2417081619697543338_n-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:60:"10685466_10203769410449195_2417081619697543338_n-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:60:"10685466_10203769410449195_2417081619697543338_n-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(46, 28, '_wp_attached_file', '2015/01/alagamar.jpg'),
(47, 28, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:960;s:6:"height";i:720;s:4:"file";s:20:"2015/01/alagamar.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"alagamar-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"alagamar-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"alagamar-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(48, 29, '_wp_attached_file', '2015/01/DSC01415.jpg'),
(49, 29, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:4912;s:6:"height";i:1080;s:4:"file";s:20:"2015/01/DSC01415.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"DSC01415-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"DSC01415-300x65.jpg";s:5:"width";i:300;s:6:"height";i:65;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"DSC01415-1024x225.jpg";s:5:"width";i:1024;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"DSC01415-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:3.2000000000000002;s:6:"credit";s:0:"";s:6:"camera";s:9:"DSC-HX300";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1406475401;s:9:"copyright";s:0:"";s:12:"focal_length";s:3:"4.3";s:3:"iso";s:2:"80";s:13:"shutter_speed";s:6:"0.0005";s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(50, 30, '_wp_attached_file', '2015/01/G0014544.jpg'),
(51, 30, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:900;s:4:"file";s:20:"2015/01/G0014544.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"G0014544-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"G0014544-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"G0014544-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"G0014544-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(52, 31, '_wp_attached_file', '2015/01/IMG_8857.jpg'),
(53, 31, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2134;s:6:"height";i:1573;s:4:"file";s:20:"2015/01/IMG_8857.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"IMG_8857-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"IMG_8857-300x221.jpg";s:5:"width";i:300;s:6:"height";i:221;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"IMG_8857-1024x754.jpg";s:5:"width";i:1024;s:6:"height";i:754;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"IMG_8857-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:2.3999999999999999;s:6:"credit";s:0:"";s:6:"camera";s:8:"iPhone 5";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1398585844;s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"4.12";s:3:"iso";s:2:"50";s:13:"shutter_speed";s:19:"0.00040600893219651";s:5:"title";s:0:"";s:11:"orientation";i:1;}}'),
(54, 32, '_wp_attached_file', '2015/01/LG3A2516.jpg'),
(55, 32, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:800;s:4:"file";s:20:"2015/01/LG3A2516.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"LG3A2516-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"LG3A2516-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"LG3A2516-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"LG3A2516-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(56, 33, '_wp_attached_file', '2015/01/LG3A2516-1.jpg'),
(57, 33, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:800;s:4:"file";s:22:"2015/01/LG3A2516-1.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"LG3A2516-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"LG3A2516-1-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"LG3A2516-1-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:22:"LG3A2516-1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(58, 34, '_wp_attached_file', '2015/01/P5100163.jpg'),
(59, 34, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:1200;s:4:"file";s:20:"2015/01/P5100163.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"P5100163-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"P5100163-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"P5100163-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"P5100163-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:4;s:6:"credit";s:0:"";s:6:"camera";s:16:"StylusTough-3000";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1399705676;s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"5";s:3:"iso";s:2:"64";s:13:"shutter_speed";s:5:"0.004";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";i:1;}}'),
(60, 35, '_wp_attached_file', '2015/01/P5100187.jpg'),
(61, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:1200;s:4:"file";s:20:"2015/01/P5100187.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"P5100187-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"P5100187-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"P5100187-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"P5100187-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:5;s:6:"credit";s:0:"";s:6:"camera";s:16:"StylusTough-3000";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1399707282;s:9:"copyright";s:0:"";s:12:"focal_length";s:4:"6.53";s:3:"iso";s:2:"64";s:13:"shutter_speed";s:7:"0.00625";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";i:1;}}'),
(62, 36, '_wp_attached_file', '2015/01/P5100193.jpg'),
(63, 36, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:1200;s:4:"file";s:20:"2015/01/P5100193.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"P5100193-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:20:"P5100193-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:21:"P5100193-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:20:"P5100193-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:4;s:6:"credit";s:0:"";s:6:"camera";s:16:"StylusTough-3000";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1399707439;s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"5";s:3:"iso";s:2:"64";s:13:"shutter_speed";s:5:"0.005";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";i:1;}}'),
(64, 37, '_wp_attached_file', '2015/01/remada-lua.jpg'),
(65, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:640;s:4:"file";s:22:"2015/01/remada-lua.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"remada-lua-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"remada-lua-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:22:"remada-lua-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(66, 38, '_wp_attached_file', '2015/01/remada-lua-1.jpg'),
(67, 38, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:640;s:4:"file";s:24:"2015/01/remada-lua-1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"remada-lua-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:24:"remada-lua-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:24:"remada-lua-1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(68, 39, '_wp_attached_file', '2015/01/sup-lua.jpg'),
(69, 39, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:640;s:4:"file";s:19:"2015/01/sup-lua.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:19:"sup-lua-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:19:"sup-lua-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:19:"sup-lua-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(70, 40, '_wp_attached_file', '2015/01/sup-lua-1.jpg'),
(71, 40, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:640;s:6:"height";i:640;s:4:"file";s:21:"2015/01/sup-lua-1.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"sup-lua-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"sup-lua-1-300x300.jpg";s:5:"width";i:300;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"sup-lua-1-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(72, 13, '_thumbnail_id', '40'),
(73, 41, '_edit_last', '1'),
(74, 41, '_edit_lock', '1421081970:1'),
(75, 41, '_thumbnail_id', '39'),
(76, 42, '_edit_last', '1'),
(77, 42, '_edit_lock', '1421081985:1'),
(78, 42, '_thumbnail_id', '40'),
(79, 43, '_edit_last', '1'),
(80, 43, '_edit_lock', '1421081995:1'),
(81, 43, '_thumbnail_id', '36'),
(82, 44, '_edit_last', '1'),
(83, 44, '_edit_lock', '1421082009:1'),
(84, 44, '_thumbnail_id', '37'),
(85, 45, '_edit_last', '1'),
(86, 45, '_edit_lock', '1421082025:1'),
(87, 45, '_thumbnail_id', '35'),
(88, 46, '_edit_last', '1'),
(89, 46, '_edit_lock', '1421082038:1'),
(90, 46, '_thumbnail_id', '34'),
(91, 47, '_edit_last', '1'),
(92, 47, '_edit_lock', '1421082057:1'),
(93, 47, '_thumbnail_id', '32'),
(94, 48, '_edit_last', '1'),
(95, 48, '_edit_lock', '1421083112:1'),
(96, 48, '_thumbnail_id', '33'),
(97, 13, '_wp_trash_meta_status', 'publish'),
(98, 13, '_wp_trash_meta_time', '1421083328'),
(99, 41, '_wp_trash_meta_status', 'publish'),
(100, 41, '_wp_trash_meta_time', '1421083328'),
(101, 42, '_wp_trash_meta_status', 'publish'),
(102, 42, '_wp_trash_meta_time', '1421083329'),
(103, 43, '_wp_trash_meta_status', 'publish'),
(104, 43, '_wp_trash_meta_time', '1421083329'),
(105, 44, '_wp_trash_meta_status', 'publish'),
(106, 44, '_wp_trash_meta_time', '1421083329'),
(107, 45, '_wp_trash_meta_status', 'publish'),
(108, 45, '_wp_trash_meta_time', '1421083329'),
(109, 46, '_wp_trash_meta_status', 'publish'),
(110, 46, '_wp_trash_meta_time', '1421083329'),
(111, 47, '_wp_trash_meta_status', 'publish'),
(112, 47, '_wp_trash_meta_time', '1421083329'),
(113, 48, '_wp_trash_meta_status', 'publish'),
(114, 48, '_wp_trash_meta_time', '1421083329'),
(115, 49, '_edit_last', '1'),
(116, 49, '_edit_lock', '1421083203:1'),
(117, 49, '_thumbnail_id', '14'),
(118, 50, '_edit_last', '1'),
(119, 50, '_edit_lock', '1421083218:1'),
(120, 50, '_thumbnail_id', '16'),
(121, 51, '_edit_last', '1'),
(122, 51, '_edit_lock', '1421083238:1'),
(123, 51, '_thumbnail_id', '18'),
(124, 52, '_edit_last', '1'),
(125, 52, '_edit_lock', '1421083286:1'),
(126, 52, '_thumbnail_id', '17'),
(127, 53, '_edit_last', '1'),
(128, 53, '_edit_lock', '1421083299:1'),
(129, 53, '_thumbnail_id', '19'),
(130, 54, '_edit_last', '1'),
(131, 54, '_edit_lock', '1421083320:1'),
(132, 54, '_thumbnail_id', '34'),
(133, 55, '_edit_last', '1'),
(134, 55, '_edit_lock', '1421083343:1'),
(135, 55, '_thumbnail_id', '38'),
(136, 56, '_thumbnail_id', '36'),
(137, 56, '_edit_last', '1'),
(138, 56, '_edit_lock', '1421083358:1'),
(139, 57, '_edit_last', '1'),
(140, 57, '_edit_lock', '1421083945:1'),
(141, 59, '_edit_last', '1'),
(144, 59, 'position', 'acf_after_title'),
(145, 59, 'layout', 'no_box'),
(146, 59, 'hide_on_screen', 'a:12:{i:0;s:9:"permalink";i:1;s:13:"custom_fields";i:2;s:10:"discussion";i:3;s:8:"comments";i:4;s:9:"revisions";i:5;s:4:"slug";i:6;s:6:"author";i:7;s:6:"format";i:8;s:14:"featured_image";i:9;s:10:"categories";i:10;s:4:"tags";i:11;s:15:"send-trackbacks";}'),
(147, 59, '_edit_lock', '1421871921:1'),
(150, 61, '_edit_last', '1'),
(151, 61, 'sobre_resumido', 'Muito mais do que uma marca, a Prosup é um movimento em pró do esporte e da qualidade de vida que usa o Stand Up Paddle como principal instrumento de trabalho.'),
(152, 61, '_sobre_resumido', 'field_54b546c9a0ef0'),
(153, 61, 'sobre', '<p>Muito mais do que uma marca, a Prosup &eacute; um movimento em pr&oacute; do esporte e da qualidade de vida que usa o Stand Up Paddle como principal instrumento de trabalho.</p><p>O grande diferencial da Prosup &eacute; a experi&ecirc;ncia da sua equipe: Paulinho Correia e Sana Busato j&aacute; eram instrutores de Sup na praia de Ponta Negra quando o esporte come&ccedil;ou a virar moda na regi&atilde;o, no ano de 2012. Tanto para Paulo quanto para Sana, introduzir os aspirantes a "supistas" ao esporte &eacute; uma miss&atilde;o de grande responsabilidade. O que define o sucesso dessa miss&atilde;o &eacute; o aluno terminar a aula completamente apaixonado pelo mar e pelo Sup.</p><p>A base da Prosup fica na praia de Ponta Negra, e ficou conhecida pelo nome "Black Point Sup", devido sua localiza&ccedil;&atilde;o (Black Point quer dizer Ponta Negra em Ingl&ecirc;s). Em uma alus&atilde;o bem humorada a este nome, um grupo de alunas formou o "Pink Point" com "meninas" (de 18 a 60 anos) que se juntam para remar em grupo, fazer passeios e travessias de Sup e para se aventurar em Sup Surf Trips.</p><p>Esta &eacute; a nossa ess&ecirc;ncia; Aproveitando o mar e paz que a natureza pode trazer, a Prosup tornou-se uma ponte para que pessoas com o mesmo interesse se encontrem. Nos grupos de remada, amizades e vida sadias s&atilde;o constru&iacute;das.</p>'),
(154, 61, '_sobre', 'field_54b4389862fe5'),
(155, 61, '_edit_lock', '1421873391:1'),
(158, 63, '_edit_last', '1'),
(162, 63, 'position', 'normal'),
(163, 63, 'layout', 'no_box'),
(164, 63, 'hide_on_screen', 'a:12:{i:0;s:9:"permalink";i:1;s:13:"custom_fields";i:2;s:10:"discussion";i:3;s:8:"comments";i:4;s:9:"revisions";i:5;s:4:"slug";i:6;s:6:"author";i:7;s:6:"format";i:8;s:14:"featured_image";i:9;s:10:"categories";i:10;s:4:"tags";i:11;s:15:"send-trackbacks";}'),
(165, 63, '_edit_lock', '1421871927:1'),
(166, 64, '_edit_last', '1'),
(167, 64, 'diferencial', 'O melhor lugar para praticar Stand Up Paddle em Natal. Atendimento de primeira e equipamento de qualidade. \r\n\r\nSe você nunca praticou SUP e deseja começar, venha aprender conosco! Contamos com os melhores e mais experientes instrutores de Natal. Além de se divertir em segurança, você vai adquirir excelente conhecimento para uma rápida evolução no esporte.\r\n\r\nSe você já rema, temos remos profissionais de carbono e fibra e pranchas para todos os gostos. \r\n\r\nVocê pode reservar sua aula ou aluguel de equipamento através do site.  Oferecemos pacotes para os usuários e alunos mais assíduos. Trabalhamos também com passeios de SUP para praias, lagoas e rios do Rio Grande do Norte.'),
(168, 64, '_diferencial', 'field_54b5498972eb5'),
(169, 64, 'diferencial_resumido', 'O melhor lugar para praticar Stand Up Paddle em Natal. Atendimento de primeira e equipamento de qualidade.'),
(170, 64, '_diferencial_resumido', 'field_54b549ed72eb6'),
(171, 64, '_edit_lock', '1421348589:1'),
(172, 65, '_edit_last', '1'),
(173, 65, 'field_54b54cdc67a13', 'a:14:{s:3:"key";s:19:"field_54b54cdc67a13";s:5:"label";s:16:"Nome do Serviço";s:4:"name";s:16:"nome_do_serviço";s:4:"type";s:4:"text";s:12:"instructions";s:16:"Nome do serviço";s:8:"required";s:1:"1";s:13:"default_value";s:0:"";s:11:"placeholder";s:0:"";s:7:"prepend";s:0:"";s:6:"append";s:0:"";s:10:"formatting";s:4:"none";s:9:"maxlength";s:0:"";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:3:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";s:5:"value";s:0:"";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(176, 65, 'position', 'acf_after_title'),
(177, 65, 'layout', 'default'),
(178, 65, 'hide_on_screen', 'a:12:{i:0;s:9:"permalink";i:1;s:7:"excerpt";i:2;s:13:"custom_fields";i:3;s:10:"discussion";i:4;s:8:"comments";i:5;s:9:"revisions";i:6;s:4:"slug";i:7;s:6:"author";i:8;s:6:"format";i:9;s:10:"categories";i:10;s:4:"tags";i:11;s:15:"send-trackbacks";}'),
(179, 65, '_edit_lock', '1421350998:1'),
(180, 66, '_thumbnail_id', '21'),
(181, 66, '_edit_last', '1'),
(182, 66, 'nome_do_serviço', 'TRAVESSIAS'),
(183, 66, '_nome_do_serviço', 'field_54b54cdc67a13'),
(184, 66, 'descricao', 'Realizamos travessias periódicas, de 4, 6 ou 10 km, de nossa base em Ponta Negra, até a Via Costeira. Para mais informações,entre em contato, e venha participe conosco da próxima aventura!'),
(185, 66, '_descricao', 'field_54b54c3d67a12'),
(186, 66, '_edit_lock', '1421169335:1'),
(187, 68, '_thumbnail_id', '19'),
(188, 68, '_edit_last', '1'),
(189, 68, 'nome_do_serviço', 'Sup Book'),
(190, 68, '_nome_do_serviço', 'field_54b54cdc67a13'),
(191, 68, 'descricao', 'Aqui na Prosup contamos com equipamento de última geração e profissionais especializados para registrar os seus melhores momentos na água. Além do Sup Book, também oferecemos serviço de gravação de vídeos.'),
(192, 68, '_descricao', 'field_54b54c3d67a12'),
(193, 68, '_edit_lock', '1421351174:1'),
(194, 70, '_edit_last', '1'),
(195, 70, '_edit_lock', '1421169221:1'),
(196, 70, 'nome_do_serviço', 'SUP Pilates'),
(197, 70, '_nome_do_serviço', 'field_54b54cdc67a13'),
(198, 70, 'descricao', 'A Prosup é pioneira em oferecer esta modalidade. Uma parceria entre professores de pilates e SUP que vai te surpreender.\r\n\r\nBenefícios do SUP Pilates:\r\n\r\n- Promove o fortalecimento muscular;\r\n- Melhora o equilíbrio e a consciência corporal;\r\n- Aperfeiçoa a respiração e concentração dos praticantes.'),
(199, 70, '_descricao', 'field_54b54c3d67a12'),
(200, 70, '_thumbnail_id', '18'),
(203, 73, '_edit_last', '1'),
(206, 73, 'position', 'acf_after_title'),
(207, 73, 'layout', 'default'),
(208, 73, 'hide_on_screen', 'a:12:{i:0;s:9:"permalink";i:1;s:7:"excerpt";i:2;s:13:"custom_fields";i:3;s:10:"discussion";i:4;s:8:"comments";i:5;s:9:"revisions";i:6;s:4:"slug";i:7;s:6:"author";i:8;s:6:"format";i:9;s:10:"categories";i:10;s:4:"tags";i:11;s:15:"send-trackbacks";}'),
(209, 73, '_edit_lock', '1421346103:1'),
(210, 65, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:8:"servicos";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(211, 74, '_edit_last', '1'),
(212, 74, '_edit_lock', '1421170360:1'),
(213, 74, '_thumbnail_id', '24'),
(214, 74, 'nome_da_aventura', 'Remada da Lua'),
(215, 74, '_nome_da_aventura', 'field_54b5545a6f9a5'),
(216, 75, '_edit_last', '1'),
(217, 75, '_edit_lock', '1421170114:1'),
(218, 75, 'nome_da_aventura', 'OUTRAS AVENTURAS'),
(219, 75, '_nome_da_aventura', 'field_54b5545a6f9a5'),
(220, 75, '_thumbnail_id', '38'),
(221, 75, '_wp_trash_meta_status', 'publish'),
(222, 75, '_wp_trash_meta_time', '1421170263'),
(223, 76, '_edit_last', '1'),
(224, 76, 'nome_da_aventura', 'Outras Aventuras'),
(225, 76, '_nome_da_aventura', 'field_54b5545a6f9a5'),
(226, 76, '_edit_lock', '1421346170:1'),
(227, 76, '_thumbnail_id', '38'),
(228, 74, '_wp_trash_meta_status', 'publish'),
(229, 74, '_wp_trash_meta_time', '1421170507'),
(230, 77, '_edit_last', '1'),
(231, 77, '_edit_lock', '1421170530:1'),
(232, 77, '_thumbnail_id', '24'),
(233, 77, 'nome_da_aventura', 'Remada da Lua'),
(234, 77, '_nome_da_aventura', 'field_54b5545a6f9a5'),
(235, 78, '_edit_last', '1'),
(236, 78, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:6:"slides";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(237, 78, 'position', 'acf_after_title'),
(238, 78, 'layout', 'default'),
(239, 78, 'hide_on_screen', 'a:13:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:10:"categories";i:11;s:4:"tags";i:12;s:15:"send-trackbacks";}'),
(240, 78, '_edit_lock', '1421258993:1'),
(241, 79, '_edit_last', '1'),
(242, 79, '_edit_lock', '1421259257:1'),
(243, 80, '_wp_attached_file', '2015/01/LG3A25161.jpg'),
(244, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:800;s:4:"file";s:21:"2015/01/LG3A25161.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"LG3A25161-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"LG3A25161-300x200.jpg";s:5:"width";i:300;s:6:"height";i:200;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"LG3A25161-1024x682.jpg";s:5:"width";i:1024;s:6:"height";i:682;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"LG3A25161-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";i:0;s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";i:0;s:9:"copyright";s:0:"";s:12:"focal_length";i:0;s:3:"iso";i:0;s:13:"shutter_speed";i:0;s:5:"title";s:0:"";s:11:"orientation";i:0;}}'),
(245, 79, '_thumbnail_id', '80'),
(246, 81, '_edit_last', '1'),
(247, 81, '_edit_lock', '1421259448:1'),
(248, 82, '_wp_attached_file', '2015/01/P51001631.jpg'),
(249, 82, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:1200;s:4:"file";s:21:"2015/01/P51001631.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"P51001631-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"P51001631-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"P51001631-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"P51001631-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:4;s:6:"credit";s:0:"";s:6:"camera";s:16:"StylusTough-3000";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1399705676;s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"5";s:3:"iso";s:2:"64";s:13:"shutter_speed";s:5:"0.004";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";i:1;}}'),
(250, 81, '_thumbnail_id', '82'),
(251, 83, '_edit_last', '1'),
(252, 83, '_edit_lock', '1421260038:1'),
(253, 84, '_wp_attached_file', '2015/01/P51001931.jpg'),
(254, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:1200;s:4:"file";s:21:"2015/01/P51001931.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"P51001931-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:21:"P51001931-300x225.jpg";s:5:"width";i:300;s:6:"height";i:225;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:22:"P51001931-1024x768.jpg";s:5:"width";i:1024;s:6:"height";i:768;s:9:"mime-type";s:10:"image/jpeg";}s:14:"post-thumbnail";a:4:{s:4:"file";s:21:"P51001931-604x270.jpg";s:5:"width";i:604;s:6:"height";i:270;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:11:{s:8:"aperture";d:4;s:6:"credit";s:0:"";s:6:"camera";s:16:"StylusTough-3000";s:7:"caption";s:0:"";s:17:"created_timestamp";i:1399707439;s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"5";s:3:"iso";s:2:"64";s:13:"shutter_speed";s:5:"0.005";s:5:"title";s:22:"OLYMPUS DIGITAL CAMERA";s:11:"orientation";i:1;}}'),
(255, 83, '_thumbnail_id', '84'),
(256, 1, '_edit_lock', '1421266583:1'),
(259, 73, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:9:"aventuras";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(261, 63, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:11:"diferencial";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(263, 96, '_edit_last', '1'),
(264, 96, 'field_54b81a8a31473', 'a:11:{s:3:"key";s:19:"field_54b81a8a31473";s:5:"label";s:4:"Link";s:4:"name";s:4:"link";s:4:"type";s:9:"page_link";s:12:"instructions";s:0:"";s:8:"required";s:1:"0";s:9:"post_type";a:1:{i:0;s:3:"all";}s:10:"allow_null";s:1:"0";s:8:"multiple";s:1:"0";s:17:"conditional_logic";a:3:{s:6:"status";s:1:"0";s:5:"rules";a:1:{i:0;a:2:{s:5:"field";s:4:"null";s:8:"operator";s:2:"==";}}s:8:"allorany";s:3:"all";}s:8:"order_no";i:0;}'),
(265, 96, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:4:"menu";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(266, 96, 'position', 'normal'),
(267, 96, 'layout', 'no_box'),
(268, 96, 'hide_on_screen', 'a:14:{i:0;s:9:"permalink";i:1;s:11:"the_content";i:2;s:7:"excerpt";i:3;s:13:"custom_fields";i:4;s:10:"discussion";i:5;s:8:"comments";i:6;s:9:"revisions";i:7;s:4:"slug";i:8;s:6:"author";i:9;s:6:"format";i:10;s:14:"featured_image";i:11;s:10:"categories";i:12;s:4:"tags";i:13;s:15:"send-trackbacks";}'),
(269, 96, '_edit_lock', '1421351596:1'),
(270, 97, '_edit_last', '1'),
(271, 97, 'link', '4'),
(272, 97, '_link', 'field_54b81a8a31473'),
(273, 97, '_edit_lock', '1421351725:1'),
(274, 98, '_edit_last', '1'),
(275, 98, 'link', '6'),
(276, 98, '_link', 'field_54b81a8a31473'),
(277, 98, '_edit_lock', '1421351780:1'),
(278, 99, '_edit_last', '1'),
(279, 99, 'link', '8'),
(280, 99, '_link', 'field_54b81a8a31473'),
(281, 99, '_edit_lock', '1421351818:1'),
(282, 100, '_edit_last', '1'),
(283, 100, 'link', '10'),
(284, 100, '_link', 'field_54b81a8a31473'),
(285, 100, '_edit_lock', '1421351846:1'),
(286, 101, '_edit_last', '1'),
(287, 101, 'link', '57'),
(288, 101, '_link', 'field_54b81a8a31473'),
(289, 101, '_edit_lock', '1421352539:1'),
(292, 61, 'link', 'sobre'),
(293, 61, '_link', 'field_54bff101e13e6'),
(295, 100, '_wp_trash_meta_status', 'publish'),
(296, 100, '_wp_trash_meta_time', '1421861181'),
(297, 101, '_wp_trash_meta_status', 'publish'),
(298, 101, '_wp_trash_meta_time', '1421861181'),
(299, 98, '_wp_trash_meta_status', 'publish'),
(300, 98, '_wp_trash_meta_time', '1421861181'),
(301, 99, '_wp_trash_meta_status', 'publish'),
(302, 99, '_wp_trash_meta_time', '1421861181'),
(303, 97, '_wp_trash_meta_status', 'publish'),
(304, 97, '_wp_trash_meta_time', '1421861181'),
(305, 103, '_edit_last', '1'),
(306, 103, 'link', '57'),
(307, 103, '_link', 'field_54b81a8a31473'),
(308, 103, '_edit_lock', '1421861112:1'),
(309, 104, '_edit_last', '1'),
(310, 104, 'link', '10'),
(311, 104, '_link', 'field_54b81a8a31473'),
(312, 104, '_edit_lock', '1421861148:1'),
(313, 105, '_edit_last', '1'),
(314, 105, 'link', '8'),
(315, 105, '_link', 'field_54b81a8a31473'),
(316, 105, '_edit_lock', '1421861177:1'),
(317, 106, '_edit_last', '1'),
(318, 106, 'link', '6'),
(319, 106, '_link', 'field_54b81a8a31473'),
(320, 106, '_edit_lock', '1421861194:1'),
(321, 107, '_edit_last', '1'),
(322, 107, 'link', '4'),
(323, 107, '_link', 'field_54b81a8a31473'),
(324, 107, '_edit_lock', '1421861282:1'),
(327, 59, 'rule', 'a:5:{s:5:"param";s:9:"post_type";s:8:"operator";s:2:"==";s:5:"value";s:5:"sobre";s:8:"order_no";i:0;s:8:"group_no";i:0;}'),
(328, 108, '_edit_last', '1'),
(329, 108, '_edit_lock', '1421874699:1'),
(330, 104, '_wp_trash_meta_status', 'publish'),
(331, 104, '_wp_trash_meta_time', '1421951826'),
(332, 103, '_wp_trash_meta_status', 'publish'),
(333, 103, '_wp_trash_meta_time', '1421951826'),
(334, 106, '_wp_trash_meta_status', 'publish'),
(335, 106, '_wp_trash_meta_time', '1421951826'),
(336, 105, '_wp_trash_meta_status', 'publish'),
(337, 105, '_wp_trash_meta_time', '1421951826'),
(338, 107, '_wp_trash_meta_status', 'publish'),
(339, 107, '_wp_trash_meta_time', '1421951826'),
(340, 110, '_edit_last', '1'),
(341, 110, 'link', '57'),
(342, 110, '_link', 'field_54b81a8a31473'),
(343, 110, '_edit_lock', '1421951795:1'),
(344, 110, '_wp_trash_meta_status', 'publish'),
(345, 110, '_wp_trash_meta_time', '1421951919'),
(346, 111, '_edit_last', '1'),
(347, 111, 'link', '4'),
(348, 111, '_link', 'field_54b81a8a31473'),
(349, 111, '_edit_lock', '1421952139:1'),
(350, 112, '_edit_last', '1'),
(351, 112, 'link', '6'),
(352, 112, '_link', 'field_54b81a8a31473'),
(353, 112, '_edit_lock', '1421952173:1'),
(354, 112, '_wp_trash_meta_status', 'publish'),
(355, 112, '_wp_trash_meta_time', '1421952292'),
(356, 111, '_wp_trash_meta_status', 'publish'),
(357, 111, '_wp_trash_meta_time', '1421952292'),
(358, 113, '_edit_last', '1'),
(359, 113, 'link', '57'),
(360, 113, '_link', 'field_54b81a8a31473'),
(361, 113, '_edit_lock', '1421952191:1'),
(362, 114, '_edit_last', '1'),
(363, 114, 'link', '108'),
(364, 114, '_link', 'field_54b81a8a31473'),
(365, 114, '_edit_lock', '1421952221:1'),
(366, 115, '_edit_last', '1'),
(367, 115, 'link', '10'),
(368, 115, '_link', 'field_54b81a8a31473'),
(369, 115, '_edit_lock', '1421952262:1'),
(370, 116, '_edit_last', '1'),
(371, 116, 'link', '8'),
(372, 116, '_link', 'field_54b81a8a31473'),
(373, 116, '_edit_lock', '1421952292:1'),
(374, 117, '_edit_last', '1'),
(375, 117, 'link', '6'),
(376, 117, '_link', 'field_54b81a8a31473'),
(377, 117, '_edit_lock', '1421952310:1'),
(378, 118, '_edit_last', '1'),
(379, 118, 'link', '4'),
(380, 118, '_link', 'field_54b81a8a31473'),
(381, 118, '_edit_lock', '1421952406:1'),
(382, 124, '_edit_last', '1'),
(383, 124, '_edit_lock', '1422046876:1'),
(384, 124, '_visibility', 'visible'),
(385, 124, '_stock_status', 'instock'),
(386, 124, 'total_sales', '0'),
(387, 124, '_downloadable', 'no'),
(388, 124, '_virtual', 'yes'),
(389, 124, '_regular_price', '20'),
(390, 124, '_sale_price', ''),
(391, 124, '_purchase_note', ''),
(392, 124, '_featured', 'no'),
(393, 124, '_weight', ''),
(394, 124, '_length', ''),
(395, 124, '_width', ''),
(396, 124, '_height', ''),
(397, 124, '_sku', ''),
(398, 124, '_product_attributes', 'a:0:{}'),
(399, 124, '_sale_price_dates_from', ''),
(400, 124, '_sale_price_dates_to', ''),
(401, 124, '_price', '20'),
(402, 124, '_sold_individually', ''),
(403, 124, '_manage_stock', 'no'),
(404, 124, '_backorders', 'no'),
(405, 124, '_stock', ''),
(406, 124, '_product_image_gallery', ''),
(407, 125, '_edit_last', '1'),
(408, 125, '_edit_lock', '1422046865:1'),
(409, 125, '_visibility', 'visible'),
(410, 125, '_stock_status', 'instock'),
(411, 125, 'total_sales', '0'),
(412, 125, '_downloadable', 'no'),
(413, 125, '_virtual', 'yes'),
(414, 125, '_regular_price', '30'),
(415, 125, '_sale_price', ''),
(416, 125, '_purchase_note', ''),
(417, 125, '_featured', 'no'),
(418, 125, '_weight', ''),
(419, 125, '_length', ''),
(420, 125, '_width', ''),
(421, 125, '_height', ''),
(422, 125, '_sku', ''),
(423, 125, '_product_attributes', 'a:0:{}'),
(424, 125, '_sale_price_dates_from', ''),
(425, 125, '_sale_price_dates_to', ''),
(426, 125, '_price', '30'),
(427, 125, '_sold_individually', ''),
(428, 125, '_manage_stock', 'no'),
(429, 125, '_backorders', 'no'),
(430, 125, '_stock', ''),
(431, 125, '_product_image_gallery', ''),
(432, 126, '_edit_last', '1'),
(433, 126, '_edit_lock', '1422046852:1'),
(434, 126, '_visibility', 'visible'),
(435, 126, '_stock_status', 'instock'),
(436, 126, 'total_sales', '0'),
(437, 126, '_downloadable', 'no'),
(438, 126, '_virtual', 'no'),
(439, 126, '_regular_price', '40'),
(440, 126, '_sale_price', ''),
(441, 126, '_purchase_note', ''),
(442, 126, '_featured', 'no'),
(443, 126, '_weight', ''),
(444, 126, '_length', ''),
(445, 126, '_width', ''),
(446, 126, '_height', ''),
(447, 126, '_sku', ''),
(448, 126, '_product_attributes', 'a:0:{}'),
(449, 126, '_sale_price_dates_from', ''),
(450, 126, '_sale_price_dates_to', ''),
(451, 126, '_price', '40'),
(452, 126, '_sold_individually', ''),
(453, 126, '_manage_stock', 'no');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(454, 126, '_backorders', 'no'),
(455, 126, '_stock', ''),
(456, 126, '_product_image_gallery', ''),
(457, 127, '_edit_last', '1'),
(458, 127, '_edit_lock', '1422046833:1'),
(459, 127, '_visibility', 'visible'),
(460, 127, '_stock_status', 'instock'),
(461, 127, 'total_sales', '0'),
(462, 127, '_downloadable', 'no'),
(463, 127, '_virtual', 'yes'),
(464, 127, '_regular_price', '200'),
(465, 127, '_sale_price', ''),
(466, 127, '_purchase_note', ''),
(467, 127, '_featured', 'no'),
(468, 127, '_weight', ''),
(469, 127, '_length', ''),
(470, 127, '_width', ''),
(471, 127, '_height', ''),
(472, 127, '_sku', ''),
(473, 127, '_product_attributes', 'a:0:{}'),
(474, 127, '_sale_price_dates_from', ''),
(475, 127, '_sale_price_dates_to', ''),
(476, 127, '_price', '200'),
(477, 127, '_sold_individually', ''),
(478, 127, '_manage_stock', 'no'),
(479, 127, '_backorders', 'no'),
(480, 127, '_stock', ''),
(481, 127, '_product_image_gallery', ''),
(482, 128, '_edit_last', '1'),
(483, 128, '_edit_lock', '1422046816:1'),
(484, 128, '_visibility', 'visible'),
(485, 128, '_stock_status', 'instock'),
(486, 128, 'total_sales', '0'),
(487, 128, '_downloadable', 'no'),
(488, 128, '_virtual', 'no'),
(489, 128, '_regular_price', '100'),
(490, 128, '_sale_price', ''),
(491, 128, '_purchase_note', ''),
(492, 128, '_featured', 'no'),
(493, 128, '_weight', ''),
(494, 128, '_length', ''),
(495, 128, '_width', ''),
(496, 128, '_height', ''),
(497, 128, '_sku', ''),
(498, 128, '_product_attributes', 'a:0:{}'),
(499, 128, '_sale_price_dates_from', ''),
(500, 128, '_sale_price_dates_to', ''),
(501, 128, '_price', '100'),
(502, 128, '_sold_individually', ''),
(503, 128, '_manage_stock', 'no'),
(504, 128, '_backorders', 'no'),
(505, 128, '_stock', ''),
(506, 128, '_product_image_gallery', ''),
(507, 120, '_edit_lock', '1422044216:1'),
(508, 121, '_edit_lock', '1422044224:1'),
(509, 129, '_billing_country', 'BR'),
(510, 129, '_billing_first_name', 'Jeffersson'),
(511, 129, '_billing_last_name', 'Araujo'),
(512, 129, '_billing_company', ''),
(513, 129, '_billing_address_1', 'asdasd'),
(514, 129, '_billing_address_2', ''),
(515, 129, '_billing_city', 'Macaíba'),
(516, 129, '_billing_state', 'RN'),
(517, 129, '_billing_postcode', '59280000'),
(518, 129, '_billing_email', 'jefferssonrenascer@gmail.com'),
(519, 129, '_billing_phone', '8432711732'),
(520, 129, '_payment_method', 'pagseguro'),
(521, 129, '_payment_method_title', 'PagSeguro'),
(522, 129, '_order_shipping', ''),
(523, 129, '_order_discount', '0'),
(524, 129, '_cart_discount', '0'),
(525, 129, '_order_tax', '0'),
(526, 129, '_order_shipping_tax', '0'),
(527, 129, '_order_total', '100.00'),
(528, 129, '_order_key', 'wc_order_54c2b9509384c'),
(529, 129, '_customer_user', '1'),
(530, 129, '_order_currency', 'BRL'),
(531, 129, '_prices_include_tax', 'no'),
(532, 129, '_customer_ip_address', '127.0.0.1'),
(533, 129, '_customer_user_agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_posts`
--

CREATE TABLE IF NOT EXISTS `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(20) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Extraindo dados da tabela `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2015-01-09 17:51:15', '2015-01-09 17:51:15', 'Bem-vindo ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a publicar!', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2015-01-09 17:51:15', '2015-01-09 17:51:15', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?p=1', 0, 'post', '', 1),
(2, 1, '2015-01-09 17:51:15', '2015-01-09 17:51:15', 'Esta é uma página de exemplo. É diferente de um post porque ela ficará em um local e será exibida na navegação do seu site (na maioria dos temas). A maioria das pessoas começa com uma página de introdução aos potenciais visitantes do site. Ela pode ser assim:\n\n<blockquote>Olá! Eu sou um bike courrier de dia, ator amador à noite e este é meu blog. Eu moro em São Paulo, tenho um cachorro chamado Tonico e eu gosto de caipirinhas. (E de ser pego pela chuva.)</blockquote>\n\nou assim:\n\n<blockquote>A XYZ foi fundada em 1971 e desde então vem proporcionando produtos de qualidade a seus clientes. Localizada em Valinhos, XYZ emprega mais de 2.000 pessoas e faz várias contribuições para a comunidade local.</blockquote>\nComo um novo usuário do WordPress, você deve ir até o <a href="http://localhost/Trabalhos/webSites/blackPoint/wp-admin/">seu painel</a> para excluir essa página e criar novas páginas com seu próprio conteúdo. Divirta-se!', 'Página de Exemplo', '', 'publish', 'open', 'open', '', 'pagina-exemplo', '', '', '2015-01-09 17:51:15', '2015-01-09 17:51:15', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=2', 0, 'page', '', 0),
(4, 1, '2015-01-09 18:24:01', '2015-01-09 18:24:01', '', 'Sobre', '', 'publish', 'open', 'open', '', 'sobre', '', '', '2015-01-09 18:24:01', '2015-01-09 18:24:01', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=4', 0, 'page', '', 0),
(5, 1, '2015-01-09 18:24:01', '2015-01-09 18:24:01', '', 'Sobre', '', 'inherit', 'open', 'open', '', '4-revision-v1', '', '', '2015-01-09 18:24:01', '2015-01-09 18:24:01', '', 4, 'http://localhost/Trabalhos/webSites/blackPoint/?p=5', 0, 'revision', '', 0),
(6, 1, '2015-01-09 18:40:28', '2015-01-09 18:40:28', '', 'Diferencial', '', 'publish', 'open', 'open', '', 'diferencial', '', '', '2015-01-09 18:40:28', '2015-01-09 18:40:28', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=6', 0, 'page', '', 0),
(7, 1, '2015-01-09 18:40:28', '2015-01-09 18:40:28', '', 'Diferencial', '', 'inherit', 'open', 'open', '', '6-revision-v1', '', '', '2015-01-09 18:40:28', '2015-01-09 18:40:28', '', 6, 'http://localhost/Trabalhos/webSites/blackPoint/6-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2015-01-09 20:25:16', '2015-01-09 20:25:16', '', 'Servicos', '', 'publish', 'open', 'open', '', 'servicos', '', '', '2015-01-09 20:25:16', '2015-01-09 20:25:16', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=8', 0, 'page', '', 0),
(9, 1, '2015-01-09 20:23:44', '2015-01-09 20:23:44', '', 'Servicos', '', 'inherit', 'open', 'open', '', '8-revision-v1', '', '', '2015-01-09 20:23:44', '2015-01-09 20:23:44', '', 8, 'http://localhost/Trabalhos/webSites/blackPoint/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2015-01-09 21:11:17', '2015-01-09 21:11:17', '', 'Aventuras', '', 'publish', 'open', 'open', '', 'aventuras', '', '', '2015-01-09 21:11:17', '2015-01-09 21:11:17', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=10', 0, 'page', '', 0),
(11, 1, '2015-01-09 21:11:17', '2015-01-09 21:11:17', '', 'Aventuras', '', 'inherit', 'open', 'open', '', '10-revision-v1', '', '', '2015-01-09 21:11:17', '2015-01-09 21:11:17', '', 10, 'http://localhost/Trabalhos/webSites/blackPoint/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2015-01-12 17:00:30', '2015-01-12 17:00:30', '', 'Campos de Galeria', '', 'publish', 'closed', 'closed', '', 'acf_campos-de-galeria', '', '', '2015-01-12 17:00:30', '2015-01-12 17:00:30', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=12', 0, 'acf', '', 0),
(13, 1, '2015-01-12 17:01:33', '2015-01-12 17:01:33', '', 'Foto 1', '', 'trash', 'closed', 'closed', '', 'foto-1', '', '', '2015-01-12 17:22:08', '2015-01-12 17:22:08', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=13', 0, 'galeria', '', 0),
(14, 1, '2015-01-12 17:01:10', '2015-01-12 17:01:10', '', '995092_10203769547772628_2306328007135427791_n', '', 'inherit', 'open', 'open', '', '995092_10203769547772628_2306328007135427791_n', '', '', '2015-01-12 17:01:10', '2015-01-12 17:01:10', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/995092_10203769547772628_2306328007135427791_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2015-01-12 17:01:11', '2015-01-12 17:01:11', '', '10013517_274328936080420_7280410948369485414_n', '', 'inherit', 'open', 'open', '', '10013517_274328936080420_7280410948369485414_n', '', '', '2015-01-12 17:01:11', '2015-01-12 17:01:11', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10013517_274328936080420_7280410948369485414_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2015-01-12 17:01:11', '2015-01-12 17:01:11', '', '10013517_274328936080420_7280410948369485414_n (1)', '', 'inherit', 'open', 'open', '', '10013517_274328936080420_7280410948369485414_n-1', '', '', '2015-01-12 17:01:11', '2015-01-12 17:01:11', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10013517_274328936080420_7280410948369485414_n-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2015-01-12 17:01:12', '2015-01-12 17:01:12', '', '10264786_275492659297381_9156587289541906779_n', '', 'inherit', 'open', 'open', '', '10264786_275492659297381_9156587289541906779_n', '', '', '2015-01-12 17:01:12', '2015-01-12 17:01:12', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10264786_275492659297381_9156587289541906779_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2015-01-12 17:01:12', '2015-01-12 17:01:12', '', '10277419_275428175970496_1040750854537516087_n', '', 'inherit', 'open', 'open', '', '10277419_275428175970496_1040750854537516087_n', '', '', '2015-01-12 17:01:12', '2015-01-12 17:01:12', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10277419_275428175970496_1040750854537516087_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2015-01-12 17:01:13', '2015-01-12 17:01:13', '', '10306248_274151272764853_372873709152478271_n', '', 'inherit', 'open', 'open', '', '10306248_274151272764853_372873709152478271_n', '', '', '2015-01-12 17:01:13', '2015-01-12 17:01:13', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10306248_274151272764853_372873709152478271_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2015-01-12 17:01:13', '2015-01-12 17:01:13', '', '10348651_274892456024068_5587817048245044284_n', '', 'inherit', 'open', 'open', '', '10348651_274892456024068_5587817048245044284_n', '', '', '2015-01-12 17:01:13', '2015-01-12 17:01:13', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10348651_274892456024068_5587817048245044284_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2015-01-12 17:01:14', '2015-01-12 17:01:14', '', '10402546_277417949104852_2759697881386835204_n', '', 'inherit', 'open', 'open', '', '10402546_277417949104852_2759697881386835204_n', '', '', '2015-01-12 17:01:14', '2015-01-12 17:01:14', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10402546_277417949104852_2759697881386835204_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2015-01-12 17:01:15', '2015-01-12 17:01:15', '', '10525997_10203382601699218_2848369748332605124_n', '', 'inherit', 'open', 'open', '', '10525997_10203382601699218_2848369748332605124_n', '', '', '2015-01-12 17:01:15', '2015-01-12 17:01:15', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10525997_10203382601699218_2848369748332605124_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2015-01-12 17:01:15', '2015-01-12 17:01:15', '', '10600661_10203769418729402_7009142274452723985_n', '', 'inherit', 'open', 'open', '', '10600661_10203769418729402_7009142274452723985_n', '', '', '2015-01-12 17:01:15', '2015-01-12 17:01:15', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10600661_10203769418729402_7009142274452723985_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2015-01-12 17:01:16', '2015-01-12 17:01:16', '', '10639605_10203769425409569_4100331693343799608_n', '', 'inherit', 'open', 'open', '', '10639605_10203769425409569_4100331693343799608_n', '', '', '2015-01-12 17:01:16', '2015-01-12 17:01:16', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10639605_10203769425409569_4100331693343799608_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2015-01-12 17:01:17', '2015-01-12 17:01:17', '', '10672270_10203769419329417_3629846643131503150_n', '', 'inherit', 'open', 'open', '', '10672270_10203769419329417_3629846643131503150_n', '', '', '2015-01-12 17:01:17', '2015-01-12 17:01:17', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10672270_10203769419329417_3629846643131503150_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2015-01-12 17:01:17', '2015-01-12 17:01:17', '', '10676362_10203769545812579_851785640718358102_n', '', 'inherit', 'open', 'open', '', '10676362_10203769545812579_851785640718358102_n', '', '', '2015-01-12 17:01:17', '2015-01-12 17:01:17', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10676362_10203769545812579_851785640718358102_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', '10685466_10203769410449195_2417081619697543338_n', '', 'inherit', 'open', 'open', '', '10685466_10203769410449195_2417081619697543338_n', '', '', '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/10685466_10203769410449195_2417081619697543338_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', 'alagamar', '', 'inherit', 'open', 'open', '', 'alagamar', '', '', '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/alagamar.jpg', 0, 'attachment', 'image/jpeg', 0),
(29, 1, '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', 'DSC01415', '', 'inherit', 'open', 'open', '', 'dsc01415', '', '', '2015-01-12 17:01:18', '2015-01-12 17:01:18', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/DSC01415.jpg', 0, 'attachment', 'image/jpeg', 0),
(30, 1, '2015-01-12 17:01:19', '2015-01-12 17:01:19', '', 'G0014544', '', 'inherit', 'open', 'open', '', 'g0014544', '', '', '2015-01-12 17:01:19', '2015-01-12 17:01:19', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/G0014544.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2015-01-12 17:01:20', '2015-01-12 17:01:20', '', 'IMG_8857', '', 'inherit', 'open', 'open', '', 'img_8857', '', '', '2015-01-12 17:01:20', '2015-01-12 17:01:20', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/IMG_8857.jpg', 0, 'attachment', 'image/jpeg', 0),
(32, 1, '2015-01-12 17:01:21', '2015-01-12 17:01:21', '', 'LG3A2516', '', 'inherit', 'open', 'open', '', 'lg3a2516', '', '', '2015-01-12 17:01:21', '2015-01-12 17:01:21', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/LG3A2516.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2015-01-12 17:01:21', '2015-01-12 17:01:21', '', 'LG3A2516 (1)', '', 'inherit', 'open', 'open', '', 'lg3a2516-1', '', '', '2015-01-12 17:01:21', '2015-01-12 17:01:21', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/LG3A2516-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(34, 1, '2015-01-12 17:01:22', '2015-01-12 17:01:22', '', 'OLYMPUS DIGITAL CAMERA', '', 'inherit', 'open', 'open', '', 'olympus-digital-camera', '', '', '2015-01-12 17:01:22', '2015-01-12 17:01:22', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/P5100163.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2015-01-12 17:01:23', '2015-01-12 17:01:23', '', 'OLYMPUS DIGITAL CAMERA', '', 'inherit', 'open', 'open', '', 'olympus-digital-camera-2', '', '', '2015-01-12 17:01:23', '2015-01-12 17:01:23', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/P5100187.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2015-01-12 17:01:24', '2015-01-12 17:01:24', '', 'OLYMPUS DIGITAL CAMERA', '', 'inherit', 'open', 'open', '', 'olympus-digital-camera-3', '', '', '2015-01-12 17:01:24', '2015-01-12 17:01:24', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/P5100193.jpg', 0, 'attachment', 'image/jpeg', 0),
(37, 1, '2015-01-12 17:01:24', '2015-01-12 17:01:24', '', 'remada lua', '', 'inherit', 'open', 'open', '', 'remada-lua', '', '', '2015-01-12 17:01:24', '2015-01-12 17:01:24', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/remada-lua.jpg', 0, 'attachment', 'image/jpeg', 0),
(38, 1, '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 'remada lua (1)', '', 'inherit', 'open', 'open', '', 'remada-lua-1', '', '', '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/remada-lua-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(39, 1, '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 'sup lua', '', 'inherit', 'open', 'open', '', 'sup-lua', '', '', '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/sup-lua.jpg', 0, 'attachment', 'image/jpeg', 0),
(40, 1, '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 'sup lua (1)', '', 'inherit', 'open', 'open', '', 'sup-lua-1', '', '', '2015-01-12 17:01:25', '2015-01-12 17:01:25', '', 13, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/sup-lua-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(41, 1, '2015-01-12 17:01:49', '2015-01-12 17:01:49', '', 'Foto 2', '', 'trash', 'closed', 'closed', '', 'foto-2', '', '', '2015-01-12 17:22:08', '2015-01-12 17:22:08', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=41', 0, 'galeria', '', 0),
(42, 1, '2015-01-12 17:02:01', '2015-01-12 17:02:01', '', 'Foto 3', '', 'trash', 'closed', 'closed', '', 'foto-3', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=42', 0, 'galeria', '', 0),
(43, 1, '2015-01-12 17:02:18', '2015-01-12 17:02:18', '', 'Foto 4', '', 'trash', 'closed', 'closed', '', 'foto-4', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=43', 0, 'galeria', '', 0),
(44, 1, '2015-01-12 17:02:31', '2015-01-12 17:02:31', '', 'Foto 5', '', 'trash', 'closed', 'closed', '', 'foto-5', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=44', 0, 'galeria', '', 0),
(45, 1, '2015-01-12 17:02:38', '2015-01-12 17:02:38', '', 'Foto 6', '', 'trash', 'closed', 'closed', '', 'foto-6', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=45', 0, 'galeria', '', 0),
(46, 1, '2015-01-12 17:03:01', '2015-01-12 17:03:01', '', 'Foto 7', '', 'trash', 'closed', 'closed', '', 'foto-7', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=46', 0, 'galeria', '', 0),
(47, 1, '2015-01-12 17:03:15', '2015-01-12 17:03:15', '', 'Foto 8', '', 'trash', 'closed', 'closed', '', 'foto-8', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=47', 0, 'galeria', '', 0),
(48, 1, '2015-01-12 17:18:05', '2015-01-12 17:18:05', '', 'Foto 9', '', 'trash', 'closed', 'closed', '', 'foto-9', '', '', '2015-01-12 17:22:09', '2015-01-12 17:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=48', 0, 'galeria', '', 0),
(49, 1, '2015-01-12 17:22:26', '2015-01-12 17:22:26', '', 'Foto 1', '', 'publish', 'closed', 'closed', '', 'foto-1-2', '', '', '2015-01-12 17:22:26', '2015-01-12 17:22:26', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=49', 0, 'galeria', '', 0),
(50, 1, '2015-01-12 17:22:40', '2015-01-12 17:22:40', '', 'Foto 2', '', 'publish', 'closed', 'closed', '', 'foto-2-2', '', '', '2015-01-12 17:22:40', '2015-01-12 17:22:40', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=50', 0, 'galeria', '', 0),
(51, 1, '2015-01-12 17:23:00', '2015-01-12 17:23:00', '', 'Foto 3', '', 'publish', 'closed', 'closed', '', 'foto-3-2', '', '', '2015-01-12 17:23:00', '2015-01-12 17:23:00', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=51', 0, 'galeria', '', 0),
(52, 1, '2015-01-12 17:23:16', '2015-01-12 17:23:16', '', 'Foto 4', '', 'publish', 'closed', 'closed', '', 'foto-4-2', '', '', '2015-01-12 17:23:16', '2015-01-12 17:23:16', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=52', 0, 'galeria', '', 0),
(53, 1, '2015-01-12 17:24:00', '2015-01-12 17:24:00', '', 'Foto 5', '', 'publish', 'closed', 'closed', '', 'foto-5-2', '', '', '2015-01-12 17:24:00', '2015-01-12 17:24:00', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=53', 0, 'galeria', '', 0),
(54, 1, '2015-01-12 17:24:23', '2015-01-12 17:24:23', '', 'Foto 6', '', 'publish', 'closed', 'closed', '', 'foto-6-2', '', '', '2015-01-12 17:24:23', '2015-01-12 17:24:23', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=54', 0, 'galeria', '', 0),
(55, 1, '2015-01-12 17:24:45', '2015-01-12 17:24:45', '', 'Foto 7', '', 'publish', 'closed', 'closed', '', 'foto-7-2', '', '', '2015-01-12 17:24:45', '2015-01-12 17:24:45', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=55', 0, 'galeria', '', 0),
(56, 1, '2015-01-12 17:24:59', '2015-01-12 17:24:59', '', 'Foto 8', '', 'publish', 'closed', 'closed', '', 'foto-8-2', '', '', '2015-01-12 17:24:59', '2015-01-12 17:24:59', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=galeria&#038;p=56', 0, 'galeria', '', 0),
(57, 1, '2015-01-12 17:34:47', '2015-01-12 17:34:47', '', 'Contato', '', 'publish', 'open', 'open', '', 'contato', '', '', '2015-01-12 17:34:47', '2015-01-12 17:34:47', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?page_id=57', 0, 'page', '', 0),
(58, 1, '2015-01-12 17:34:47', '2015-01-12 17:34:47', '', 'Contato', '', 'inherit', 'open', 'open', '', '57-revision-v1', '', '', '2015-01-12 17:34:47', '2015-01-12 17:34:47', '', 57, 'http://localhost/Trabalhos/webSites/blackPoint/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2015-01-12 21:12:41', '2015-01-12 21:12:41', '', 'Campos Sobre', '', 'publish', 'closed', 'closed', '', 'acf_campos-sobre', '', '', '2015-01-21 20:25:21', '2015-01-21 20:25:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=59', 0, 'acf', '', 0),
(61, 1, '2015-01-13 16:29:51', '2015-01-13 16:29:51', '<!--:pt-->Muito mais do que uma marca, a Prosup é um movimento em pró do esporte e da qualidade de vida que usa o Stand Up Paddle como principal instrumento de trabalho.<!--:--><!--:en-->Much more than a brand, PROSUP is a movement in pro sports and quality of life using the Stand Up Paddle main working tool.<!--:--><!--more--><!--:pt-->\r\n\r\nO grande diferencial da Prosup é a experiência da sua equipe: Paulinho Correia e Sana Busato já eram instrutores de Sup na praia de Ponta Negra quando o esporte começou a virar moda na região, no ano de 2012. Tanto para Paulo quanto para Sana, introduzir os aspirantes a “supistas” ao esporte é uma missão de grande responsabilidade. O que define o sucesso dessa missão é o aluno terminar a aula completamente apaixonado pelo mar e pelo Sup.\r\n\r\nA base da Prosup fica na praia de Ponta Negra, e ficou conhecida pelo nome “Black Point Sup”, devido sua localização (Black Point quer dizer Ponta Negra em Inglês). Em uma alusão bem humorada a este nome, um grupo de alunas formou o “Pink Point” com “meninas” (de 18 a 60 anos) que se juntam para remar em grupo, fazer passeios e travessias de Sup e para se aventurar em Sup Surf Trips.\r\n\r\nEsta é a nossa essência; Aproveitando o mar e paz que a natureza pode trazer, a Prosup tornou-se uma ponte para que pessoas com o mesmo interesse se encontrem. Nos grupos de remada, amizades e vida sadias são construídas.<!--:--><!--:en-->\r\n\r\nThe great advantage of PROSUP is the experience of its team: Paulinho Correia and Sana Busato were already Sup instructors on Ponta Negra beach when the sport began to turn fashion in the region in the year 2012. So much for Paul and for Sana, enter aspiring "supistas" sport is a task of great responsibility. What defines the success of this mission is the student finish the class completely in love by the sea and Sup.\r\n\r\nThe basis of PROSUP is on Ponta Negra beach, and was known as the "Black Point Sup", because of its location (Black Point means Ponta Negra in English). In a humorous allusion to this name, a group of students formed the "Pink Point" with "girls" (18-60 years) who join to paddle in a group, take tours and crossings Sup and to venture into Sup surf Trips.\r\n\r\nThis is our essence; Enjoying the sea and peace that nature can bring, the PROSUP became a bridge for people with the same interests are. In rowing groups, healthy friendships and life are built.<!--:-->', '<!--:en-->Sobre Nós<!--:--><!--:pt-->Sobre Nós<!--:--><!--:es-->Sobre Nós<!--:-->', '', 'publish', 'closed', 'closed', '', '61-2', '', '', '2015-01-21 18:34:17', '2015-01-21 18:34:17', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=sobre&#038;p=61', 0, 'sobre', '', 0),
(63, 1, '2015-01-13 16:39:13', '2015-01-13 16:39:13', '', 'Campos Diferencial', '', 'publish', 'closed', 'closed', '', 'acf_campos-diferencial', '', '', '2015-01-15 18:26:05', '2015-01-15 18:26:05', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=63', 0, 'acf', '', 0),
(64, 1, '2015-01-13 16:39:36', '2015-01-13 16:39:36', '<!--:pt-->O melhor lugar para praticar Stand Up Paddle em Natal. Atendimento de primeira e equipamento de qualidade. <!--:--><!--more--><!--:pt-->\r\n \r\nSe você nunca praticou SUP e deseja começar, venha aprender conosco! Contamos com os melhores e mais experientes instrutores de Natal. Além de se divertir em segurança, você vai adquirir excelente conhecimento para uma rápida evolução no esporte.\r\n \r\nSe você já rema, temos remos profissionais de carbono e fibra e pranchas para todos os gostos. \r\n \r\nVocê pode reservar sua aula ou aluguel de equipamento através do site.  Oferecemos pacotes para os usuários e alunos mais assíduos. Trabalhamos também com passeios de SUP para praias, lagoas e rios do Rio Grande do Norte.<!--:-->', '<!--:pt-->Diferencial<!--:--><!--:en-->Diferencial<!--:--><!--:es-->Diferencial<!--:-->', '', 'publish', 'closed', 'closed', '', '64-2', '', '', '2015-01-15 19:03:38', '2015-01-15 19:03:38', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=diferencial&#038;p=64', 0, 'diferencial', '', 0),
(65, 1, '2015-01-13 16:51:32', '2015-01-13 16:51:32', '', 'Campos Serviços', '', 'publish', 'closed', 'closed', '', 'acf_campos-servicos', '', '', '2015-01-13 17:24:36', '2015-01-13 17:24:36', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=65', 0, 'acf', '', 0),
(66, 1, '2015-01-13 16:52:40', '2015-01-13 16:52:40', '<p class="western">Realizamos travessias periódicas, de 4, 6 ou 10 km, de nossa base em Ponta Negra, até a Via Costeira. Para mais informações, entre em contato, e venha participe conosco da próxima aventura!</p>', 'TRAVESSIAS', '', 'publish', 'closed', 'closed', '', '66-2', '', '', '2015-01-13 17:17:34', '2015-01-13 17:17:34', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=servicos&#038;p=66', 0, 'servicos', '', 0),
(68, 1, '2015-01-13 17:03:03', '2015-01-13 17:03:03', '<!--:pt--><p class="western">Aqui na Prosup contamos com equipamento de última geração e profissionais especializados para registrar os seus melhores momentos na água. Além do Sup Book, também oferecemos serviço de gravação de vídeos.</p><!--:--><!--:en--><p class="western">Aqui na Prosup contamos com equipamento de última geração e profissionais especializados para registrar os seus melhores momentos na água. Além do Sup Book, também oferecemos serviço de gravação de vídeos.</p><!--:--><!--:es--><p class="western">Aqui na Prosup contamos com equipamento de última geração e profissionais especializados para registrar os seus melhores momentos na água. Além do Sup Book, também oferecemos serviço de gravação de vídeos.</p><!--:-->', 'Sup Book', '', 'publish', 'closed', 'closed', '', '68-2', '', '', '2015-01-15 19:44:20', '2015-01-15 19:44:20', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=servicos&#038;p=68', 0, 'servicos', '', 0),
(70, 1, '2015-01-13 17:04:21', '2015-01-13 17:04:21', 'A Prosup é pioneira em oferecer esta modalidade. Uma parceria entre professores de  pilates e SUP que vai te surpreender. Benefícios do SUP Pilates:\r\n<ul>\r\n	<li>Promove o fortalecimento muscular;</li>\r\n	<li>Melhora o equilíbrio e a consciência corporal;</li>\r\n	<li>Aperfeiçoa a respiração e concentração dos praticantes.</li>\r\n</ul>', 'SUP Pilates', '', 'publish', 'closed', 'closed', '', 'sup-pilates', '', '', '2015-01-13 17:13:58', '2015-01-13 17:13:58', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=servicos&#038;p=70', 0, 'servicos', '', 0),
(72, 1, '2015-01-13 17:17:09', '2015-01-13 17:17:09', '<p class="western"><span style="color: #222222;"><span style="font-family: Arial, sans-serif;"><span style="font-size: medium;">Realizamos travessias periódicas, de 4, 6 ou 10 km, de nossa base em Ponta Negra, até a Via Costeira. Para mais informações, entre em contato</span></span></span><span style="color: #222222;"><span style="font-family: Arial, sans-serif;"><span style="font-size: medium;">, e venha participe conosco da próxima aventura! </span></span></span></p>', 'TRAVESSIAS', '', 'inherit', 'open', 'open', '', '66-autosave-v1', '', '', '2015-01-13 17:17:09', '2015-01-13 17:17:09', '', 66, 'http://localhost/Trabalhos/webSites/blackPoint/66-autosave-v1/', 0, 'revision', '', 0),
(73, 1, '2015-01-13 17:24:10', '2015-01-13 17:24:10', '', 'Campos Aventuras', '', 'publish', 'closed', 'closed', '', 'acf_campos-aventuras', '', '', '2015-01-15 18:21:43', '2015-01-15 18:21:43', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=73', 0, 'acf', '', 0),
(74, 1, '2015-01-13 17:25:27', '2015-01-13 17:25:27', 'Tradicionalmente a cada lua cheia a Prosup realiza a Remada da Lua para saudar o nascimento da lua, de dentro do mar. Depois da atividade, nada melhor que um piquenique aconchegante ao redor da fogueira para confraternizar e trocar experiências.\r\n\r\nA Remada da Lua é um encontro aberto a todos os amantes do luar. Para participar, basta ter alguma experiência no SUP. Entre em contato e desfrute deste belo encontro com a natureza!', 'Remada da Lua', '', 'trash', 'closed', 'closed', '', 'remada-da-lua', '', '', '2015-01-13 17:35:07', '2015-01-13 17:35:07', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=aventuras&#038;p=74', 0, 'aventuras', '', 0),
(75, 1, '2015-01-13 17:27:30', '2015-01-13 17:27:30', '<div class="row">\r\n<div class="row">\r\n<div class="col-sm-6 divServicos">\r\n<p class="western">Na intenção de renovar a energia e o astral da galera que curte o esporte, a Prosup desenvolve diversos eventos e atividades ao longo do ano. Esses eventos são sempre um bom motivo para que todos os amantes do esporte, iniciantes ou praticantes de longa data, se encontrem e compartilhem de bons momentos.</p>\r\n<p class="western">Com essa intenção, a Prosup aproveita datas importantes do calendário, como Dia dos Pais ou Dia das Crianças, e promove atividades que ajudam a divulgar o esporte e a data ou causa envolvida. Realizamos remadas solidarias e de conscientização, como mutirões de coleta de lixo pela praia ou incentivando causas nobres, como é o caso da Remada Rosa, que acontece no mês de outubro em apoio à luta Contra o Câncer de Mama.</p>\r\n<p class="western">Ao longo do ano a Prosup também promove competições de Stand Up Paddle, para incentivar o crescimento do esporte no Rio Grande do Norte.</p>\r\n<p class="western">Fique atento! Estamos sempre na atividade e esperando por você.</p>\r\n\r\n</div>\r\n</div>\r\n</div>', 'OUTRAS AVENTURAS', '', 'trash', 'closed', 'closed', '', 'outras-aventuras', '', '', '2015-01-13 17:31:03', '2015-01-13 17:31:03', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=aventuras&#038;p=75', 0, 'aventuras', '', 0),
(76, 1, '2015-01-13 17:34:07', '2015-01-13 17:34:07', '<!--:pt--><p class="western">Na intenção de renovar a energia e o astral da galera que curte o esporte, a Prosup desenvolve diversos eventos e atividades ao longo do ano. Esses eventos são sempre um bom motivo para que todos os amantes do esporte, iniciantes ou praticantes de longa data, se encontrem e compartilhem de bons momentos.</p>\r\n<p class="western">Com essa intenção, a Prosup aproveita datas importantes do calendário, como Dia dos Pais ou Dia das Crianças, e promove atividades que ajudam a divulgar o esporte e a data ou causa envolvida. Realizamos remadas solidarias e de conscientização, como mutirões de coleta de lixo pela praia ou incentivando causas nobres, como é o caso da Remada Rosa, que acontece no mês de outubro em apoio à luta Contra o Câncer de Mama.</p>\r\n<p class="western">Ao longo do ano a Prosup também promove competições de Stand Up Paddle, para incentivar o crescimento do esporte no Rio Grande do Norte.</p>\r\n<p class="western">Fique atento! Estamos sempre na atividade e esperando por você.</p><!--:--><!--:en--><p class="western">Na intenção de renovar a energia e o astral da galera que curte o esporte, a Prosup desenvolve diversos eventos e atividades ao longo do ano. Esses eventos são sempre um bom motivo para que todos os amantes do esporte, iniciantes ou praticantes de longa data, se encontrem e compartilhem de bons momentos.</p>\r\n<p class="western">Com essa intenção, a Prosup aproveita datas importantes do calendário, como Dia dos Pais ou Dia das Crianças, e promove atividades que ajudam a divulgar o esporte e a data ou causa envolvida. Realizamos remadas solidarias e de conscientização, como mutirões de coleta de lixo pela praia ou incentivando causas nobres, como é o caso da Remada Rosa, que acontece no mês de outubro em apoio à luta Contra o Câncer de Mama.</p>\r\n<p class="western">Ao longo do ano a Prosup também promove competições de Stand Up Paddle, para incentivar o crescimento do esporte no Rio Grande do Norte.</p>\r\n<p class="western">Fique atento! Estamos sempre na atividade e esperando por você.</p><!--:--><!--:es--><p class="western">Na intenção de renovar a energia e o astral da galera que curte o esporte, a Prosup desenvolve diversos eventos e atividades ao longo do ano. Esses eventos são sempre um bom motivo para que todos os amantes do esporte, iniciantes ou praticantes de longa data, se encontrem e compartilhem de bons momentos.</p>\r\n<p class="western">Com essa intenção, a Prosup aproveita datas importantes do calendário, como Dia dos Pais ou Dia das Crianças, e promove atividades que ajudam a divulgar o esporte e a data ou causa envolvida. Realizamos remadas solidarias e de conscientização, como mutirões de coleta de lixo pela praia ou incentivando causas nobres, como é o caso da Remada Rosa, que acontece no mês de outubro em apoio à luta Contra o Câncer de Mama.</p>\r\n<p class="western">Ao longo do ano a Prosup também promove competições de Stand Up Paddle, para incentivar o crescimento do esporte no Rio Grande do Norte.</p>\r\n<p class="western">Fique atento! Estamos sempre na atividade e esperando por você.</p><!--:-->', '<!--:pt-->Outras Aventuras<!--:--><!--:en-->Never Jamais<!--:--><!--:es-->Outras Aventuras<!--:-->', '', 'publish', 'closed', 'closed', '', 'outras-aventuras-2', '', '', '2015-01-15 18:22:09', '2015-01-15 18:22:09', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=aventuras&#038;p=76', 0, 'aventuras', '', 0),
(77, 1, '2015-01-13 17:35:30', '2015-01-13 17:35:30', 'Tradicionalmente a cada lua cheia a Prosup realiza a Remada da Lua para saudar o nascimento da lua, de dentro do mar. Depois da atividade, nada melhor que um piquenique aconchegante ao redor da fogueira para confraternizar e trocar experiências.  A Remada da Lua é um encontro aberto a todos os amantes do luar. Para participar, basta ter alguma experiência no SUP. Entre em contato e desfrute deste belo encontro com a natureza!', 'Remada da Lua', '', 'publish', 'closed', 'closed', '', 'remada-da-lua-2', '', '', '2015-01-13 17:35:30', '2015-01-13 17:35:30', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=aventuras&#038;p=77', 0, 'aventuras', '', 0),
(78, 1, '2015-01-14 18:12:12', '2015-01-14 18:12:12', '', 'Campos de Slides', '', 'publish', 'closed', 'closed', '', 'acf_campos-de-slides', '', '', '2015-01-14 18:12:12', '2015-01-14 18:12:12', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=78', 0, 'acf', '', 0),
(79, 1, '2015-01-14 18:15:31', '2015-01-14 18:15:31', '', 'Slide 1', '', 'publish', 'closed', 'closed', '', 'slide-1', '', '', '2015-01-14 18:15:31', '2015-01-14 18:15:31', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=slides&#038;p=79', 0, 'slides', '', 0),
(80, 1, '2015-01-14 18:15:25', '2015-01-14 18:15:25', '', 'LG3A2516', '', 'inherit', 'open', 'open', '', 'lg3a2516-2', '', '', '2015-01-14 18:15:25', '2015-01-14 18:15:25', '', 79, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/LG3A25161.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2015-01-14 18:17:02', '2015-01-14 18:17:02', '', 'Slide 2', '', 'publish', 'closed', 'closed', '', 'slide-2', '', '', '2015-01-14 18:17:02', '2015-01-14 18:17:02', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=slides&#038;p=81', 0, 'slides', '', 0),
(82, 1, '2015-01-14 18:16:57', '2015-01-14 18:16:57', '', 'OLYMPUS DIGITAL CAMERA', '', 'inherit', 'open', 'open', '', 'olympus-digital-camera-4', '', '', '2015-01-14 18:16:57', '2015-01-14 18:16:57', '', 81, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/P51001631.jpg', 0, 'attachment', 'image/jpeg', 0),
(83, 1, '2015-01-14 18:20:10', '2015-01-14 18:20:10', '', 'Slide 3', '', 'publish', 'closed', 'closed', '', 'slide-3', '', '', '2015-01-14 18:20:10', '2015-01-14 18:20:10', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=slides&#038;p=83', 0, 'slides', '', 0),
(84, 1, '2015-01-14 18:20:07', '2015-01-14 18:20:07', '', 'OLYMPUS DIGITAL CAMERA', '', 'inherit', 'open', 'open', '', 'olympus-digital-camera-5', '', '', '2015-01-14 18:20:07', '2015-01-14 18:20:07', '', 83, 'http://localhost/Trabalhos/webSites/blackPoint/wp-content/uploads/2015/01/P51001931.jpg', 0, 'attachment', 'image/jpeg', 0),
(96, 1, '2015-01-15 19:53:15', '2015-01-15 19:53:15', '', 'Campos Menu', '', 'publish', 'closed', 'closed', '', 'acf_campos-menu', '', '', '2015-01-15 19:53:15', '2015-01-15 19:53:15', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=acf&#038;p=96', 0, 'acf', '', 0),
(97, 1, '2015-01-15 19:54:19', '2015-01-15 19:54:19', '', '<!--:pt-->SOBRE<!--:--><!--:en-->SOBRE<!--:--><!--:es-->SOBRE<!--:-->', '', 'trash', 'closed', 'closed', '', 'sobre-2', '', '', '2015-01-21 17:26:21', '2015-01-21 17:26:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=menu&#038;p=97', 0, 'menu', '', 0),
(98, 1, '2015-01-15 19:57:50', '2015-01-15 19:57:50', '', '<!--:pt-->DIFERENCIAL<!--:--><!--:en-->DIFERENCIAL<!--:--><!--:es-->DIFERENCIAL<!--:-->', '', 'trash', 'closed', 'closed', '', 'diferencial-2', '', '', '2015-01-21 17:26:21', '2015-01-21 17:26:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=menu&#038;p=98', 0, 'menu', '', 0),
(99, 1, '2015-01-15 19:58:31', '2015-01-15 19:58:31', '', '<!--:pt-->SERVIÇOS<!--:--><!--:en-->SERVIÇOS<!--:--><!--:es-->SERVIÇOS<!--:-->', '', 'trash', 'closed', 'closed', '', 'servicos-2', '', '', '2015-01-21 17:26:21', '2015-01-21 17:26:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=menu&#038;p=99', 0, 'menu', '', 0),
(100, 1, '2015-01-15 19:59:01', '2015-01-15 19:59:01', '', '<!--:pt-->AVENTURAS<!--:--><!--:en-->AVENTURAS<!--:--><!--:es-->AVENTURAS<!--:-->', '', 'trash', 'closed', 'closed', '', 'aventuras-2', '', '', '2015-01-21 17:26:21', '2015-01-21 17:26:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=menu&#038;p=100', 0, 'menu', '', 0),
(101, 1, '2015-01-15 20:00:45', '2015-01-15 20:00:45', '', '<!--:pt-->CONTATO<!--:--><!--:en-->CONTATO<!--:--><!--:es-->CONTATO<!--:-->', '', 'trash', 'closed', 'closed', '', 'contato-2', '', '', '2015-01-21 17:26:21', '2015-01-21 17:26:21', '', 0, 'http://localhost/Trabalhos/webSites/blackPoint/?post_type=menu&#038;p=101', 0, 'menu', '', 0),
(102, 1, '2015-01-21 17:26:09', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2015-01-21 17:26:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/blackpoint/?p=102', 0, 'post', '', 0),
(103, 1, '2015-01-21 17:26:59', '2015-01-21 17:26:59', '', '<!--:pt-->CONTATO<!--:--><!--:en-->CONTATO<!--:--><!--:es-->CONTATO<!--:-->', '', 'trash', 'closed', 'closed', '', 'contato-3', '', '', '2015-01-22 18:37:06', '2015-01-22 18:37:06', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=103', 0, 'menu', '', 0),
(104, 1, '2015-01-21 17:27:35', '2015-01-21 17:27:35', '', '<!--:pt-->AVENTURAS<!--:--><!--:en-->AVENTURAS<!--:--><!--:es-->AVENTURAS<!--:-->', '', 'trash', 'closed', 'closed', '', 'aventuras-3', '', '', '2015-01-22 18:37:06', '2015-01-22 18:37:06', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=104', 0, 'menu', '', 0),
(105, 1, '2015-01-21 17:28:02', '2015-01-21 17:28:02', '', '<!--:pt-->SERVIÇOS<!--:--><!--:en-->SERVIÇOS<!--:--><!--:es-->SERVIÇOS<!--:-->', '', 'trash', 'closed', 'closed', '', 'servicos-3', '', '', '2015-01-22 18:37:06', '2015-01-22 18:37:06', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=105', 0, 'menu', '', 0),
(106, 1, '2015-01-21 17:28:20', '2015-01-21 17:28:20', '', '<!--:pt-->DIFERENCIAL<!--:--><!--:en-->DIFERENCIAL<!--:--><!--:es-->DIFERENCIAL<!--:-->', '', 'trash', 'closed', 'closed', '', 'diferencial-3', '', '', '2015-01-22 18:37:06', '2015-01-22 18:37:06', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=106', 0, 'menu', '', 0),
(107, 1, '2015-01-21 17:28:42', '2015-01-21 17:28:42', '', '<!--:pt-->SOBRE<!--:--><!--:en-->SOBRE<!--:--><!--:es-->SOBRE<!--:-->', '', 'trash', 'closed', 'closed', '', 'sobre-3', '', '', '2015-01-22 18:37:06', '2015-01-22 18:37:06', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=107', 0, 'menu', '', 0),
(108, 1, '2015-01-21 20:55:13', '2015-01-21 20:55:13', '', '<!--:pt-->Agendamento<!--:--><!--:en-->Agendamento<!--:--><!--:es-->Agendamento<!--:-->', '', 'publish', 'open', 'open', '', 'agendamento', '', '', '2015-01-21 20:55:13', '2015-01-21 20:55:13', '', 0, 'http://localhost/blackpoint/?page_id=108', 0, 'page', '', 0),
(109, 1, '2015-01-21 20:55:13', '2015-01-21 20:55:13', '', '<!--:pt-->Agendamento<!--:--><!--:en-->Agendamento<!--:--><!--:es-->Agendamento<!--:-->', '', 'inherit', 'open', 'open', '', '108-revision-v1', '', '', '2015-01-21 20:55:13', '2015-01-21 20:55:13', '', 108, 'http://localhost/blackpoint/108-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2015-01-22 18:38:20', '2015-01-22 18:38:20', '', '<!--:pt-->CONTATO<!--:--><!--:en-->CONTATO<!--:--><!--:es-->CONTATO<!--:-->', '', 'trash', 'closed', 'closed', '', 'contato-4', '', '', '2015-01-22 18:38:39', '2015-01-22 18:38:39', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=110', 0, 'menu', '', 0),
(111, 1, '2015-01-22 18:44:04', '2015-01-22 18:44:04', '', '<!--:pt-->SOBRE<!--:--><!--:en-->SOBRE<!--:--><!--:es-->SOBRE<!--:-->', '', 'trash', 'closed', 'closed', '', 'sobre-4', '', '', '2015-01-22 18:44:52', '2015-01-22 18:44:52', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=111', 0, 'menu', '', 0),
(112, 1, '2015-01-22 18:44:39', '2015-01-22 18:44:39', '', '<!--:pt-->DIFERENCIAL<!--:--><!--:en-->DIFERENCIAL<!--:--><!--:es-->DIFERENCIAL<!--:-->', '', 'trash', 'closed', 'closed', '', 'diferencial-4', '', '', '2015-01-22 18:44:52', '2015-01-22 18:44:52', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=112', 0, 'menu', '', 0),
(113, 1, '2015-01-22 18:45:04', '2015-01-22 18:45:04', '', '<!--:pt-->CONTATO<!--:--><!--:en-->CONTATO<!--:--><!--:es-->CONTATO<!--:-->', '', 'publish', 'closed', 'closed', '', 'contato-5', '', '', '2015-01-22 18:45:04', '2015-01-22 18:45:04', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=113', 0, 'menu', '', 0),
(114, 1, '2015-01-22 18:45:23', '2015-01-22 18:45:23', '', '<!--:pt-->AGENDAMENTO<!--:--><!--:en-->AGENDAMENTO<!--:--><!--:es-->AGENDAMENTO<!--:-->', '', 'publish', 'closed', 'closed', '', 'agendamento-2', '', '', '2015-01-22 18:45:23', '2015-01-22 18:45:23', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=114', 0, 'menu', '', 0),
(115, 1, '2015-01-22 18:45:59', '2015-01-22 18:45:59', '', '<!--:pt-->AVENTURAS<!--:--><!--:en-->AVENTURAS<!--:--><!--:es-->AVENTURAS<!--:-->', '', 'publish', 'closed', 'closed', '', 'aventuras-4', '', '', '2015-01-22 18:45:59', '2015-01-22 18:45:59', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=115', 0, 'menu', '', 0),
(116, 1, '2015-01-22 18:46:38', '2015-01-22 18:46:38', '', '<!--:pt-->SERVIÇOS<!--:--><!--:en-->SERVIÇOS<!--:--><!--:es-->SERVIÇOS<!--:-->', '', 'publish', 'closed', 'closed', '', 'servicos-4', '', '', '2015-01-22 18:46:38', '2015-01-22 18:46:38', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=116', 0, 'menu', '', 0),
(117, 1, '2015-01-22 18:47:03', '2015-01-22 18:47:03', '', '<!--:pt-->DIFERENCIAL<!--:--><!--:en-->DIFERENCIAL<!--:--><!--:es-->DIFERENCIAL<!--:-->', '', 'publish', 'closed', 'closed', '', 'diferencial-5', '', '', '2015-01-22 18:47:03', '2015-01-22 18:47:03', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=117', 0, 'menu', '', 0),
(118, 1, '2015-01-22 18:47:20', '2015-01-22 18:47:20', '', '<!--:pt-->SOBRE<!--:--><!--:en-->SOBRE<!--:--><!--:es-->SOBRE<!--:-->', '', 'publish', 'closed', 'closed', '', 'sobre-5', '', '', '2015-01-22 18:47:20', '2015-01-22 18:47:20', '', 0, 'http://localhost/blackpoint/?post_type=menu&#038;p=118', 0, 'menu', '', 0),
(119, 1, '2015-01-22 20:03:41', '2015-01-22 20:03:41', '', 'Loja', '', 'publish', 'closed', 'open', '', 'loja', '', '', '2015-01-22 20:03:41', '2015-01-22 20:03:41', '', 0, 'http://localhost/blackpoint/loja/', 0, 'page', '', 0),
(120, 1, '2015-01-22 20:03:41', '2015-01-22 20:03:41', '[woocommerce_cart]', 'Carrinho', '', 'publish', 'closed', 'open', '', 'carrinho', '', '', '2015-01-22 20:03:41', '2015-01-22 20:03:41', '', 0, 'http://localhost/blackpoint/carrinho/', 0, 'page', '', 0),
(121, 1, '2015-01-22 20:03:41', '2015-01-22 20:03:41', '[woocommerce_checkout]', 'Finalizar compra', '', 'publish', 'closed', 'open', '', 'finalizar-compra', '', '', '2015-01-22 20:03:41', '2015-01-22 20:03:41', '', 0, 'http://localhost/blackpoint/finalizar-compra/', 0, 'page', '', 0),
(122, 1, '2015-01-22 20:03:41', '2015-01-22 20:03:41', '[woocommerce_my_account]', 'Minha conta', '', 'publish', 'closed', 'open', '', 'minha-conta', '', '', '2015-01-22 20:03:41', '2015-01-22 20:03:41', '', 0, 'http://localhost/blackpoint/minha-conta/', 0, 'page', '', 0),
(123, 1, '2015-01-22 20:03:47', '2015-01-22 20:03:47', '<h1>Checkout</h1>\n\n{{mj-checkout-form}}', 'Mijireh Secure Checkout', '', 'private', 'closed', 'closed', '', 'mijireh-secure-checkout', '', '', '2015-01-22 20:03:47', '2015-01-22 20:03:47', '', 0, 'http://localhost/blackpoint/mijireh-secure-checkout/', 0, 'page', '', 0),
(124, 1, '2015-01-22 20:49:49', '2015-01-22 20:49:49', '<!--:pt-->Locação de prancha e remo para prática de stand-up paddle.<!--:--><!--:en-->Locação de prancha e remo para prática de stand-up paddle.<!--:--><!--:es-->Locação de prancha e remo para prática de stand-up paddle.<!--:-->', '<!--:pt-->Locação de Prancha (30min)<!--:-->', '', 'publish', 'open', 'closed', '', 'locacao-de-prancha', '', '', '2015-01-23 21:03:08', '2015-01-23 21:03:08', '', 0, 'http://localhost/blackpoint/?post_type=product&#038;p=124', 0, 'product', '', 0),
(125, 1, '2015-01-23 18:26:38', '2015-01-23 18:26:38', '<!--:pt-->Locação de prancha e remo para prática de stand-up paddle.<!--:--><!--:en-->Locação de prancha e remo para prática de stand-up paddle.<!--:--><!--:es-->Locação de prancha e remo para prática de stand-up paddle.<!--:-->', '<!--:pt-->Aluguel de Prancha (60 min)<!--:-->', '', 'publish', 'open', 'closed', '', 'aluguel-de-prancha-60-min', '', '', '2015-01-23 21:02:54', '2015-01-23 21:02:54', '', 0, 'http://localhost/blackpoint/?post_type=product&#038;p=125', 0, 'product', '', 0),
(126, 1, '2015-01-23 18:41:56', '2015-01-23 18:41:56', '<!--:pt-->Aulas práticas de stand paddle com acompanhamento de instrutor.<!--:-->', '<!--:pt-->01 Hora de Aula<!--:-->', '', 'publish', 'open', 'closed', '', '01-hora-de-aula', '', '', '2015-01-23 21:02:35', '2015-01-23 21:02:35', '', 0, 'http://localhost/blackpoint/?post_type=product&#038;p=126', 0, 'product', '', 0),
(127, 1, '2015-01-23 18:44:52', '2015-01-23 18:44:52', '<!--:pt-->Pacote com 10 aulas de Stand Up paddle. Aula com 60 min de duração.<!--:-->', '<!--:pt-->Pacote com 10 aulas<!--:-->', '', 'publish', 'open', 'closed', '', 'pacote-com-10-aulas', '', '', '2015-01-23 21:02:19', '2015-01-23 21:02:19', '', 0, 'http://localhost/blackpoint/?post_type=product&#038;p=127', 0, 'product', '', 0),
(128, 1, '2015-01-23 18:46:54', '2015-01-23 18:46:54', '<!--:pt-->Stamp Paddle sobre o luar.<!--:-->', '<!--:pt-->Remada na lua<!--:-->', '', 'publish', 'open', 'closed', '', 'remada-na-lua', '', '', '2015-01-23 21:02:02', '2015-01-23 21:02:02', '', 0, 'http://localhost/blackpoint/?post_type=product&#038;p=128', 0, 'product', '', 0),
(129, 1, '2015-01-23 21:08:58', '2015-01-23 21:08:58', '', 'Pedido &ndash;  23 de Jan de 2015 às 21:12:12', '', 'publish', 'open', 'closed', 'order_54c2b9508f345', 'pedido-23-de-jan-de-2015-as-210808', '', '', '2015-01-26 17:29:54', '2015-01-26 17:29:54', '', 0, 'http://localhost/blackpoint/?post_type=shop_order&#038;p=129', 0, 'shop_order', '', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_terms`
--

CREATE TABLE IF NOT EXISTS `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'pending', 'pending', 0),
(7, 'failed', 'failed', 0),
(8, 'on-hold', 'on-hold', 0),
(9, 'processing', 'processing', 0),
(10, 'completed', 'completed', 0),
(11, 'refunded', 'refunded', 0),
(12, 'cancelled', 'cancelled', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_relationships`
--

CREATE TABLE IF NOT EXISTS `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(124, 2, 0),
(125, 2, 0),
(126, 2, 0),
(127, 2, 0),
(128, 2, 0),
(129, 12, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 5),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'shop_order_status', '', 0, 0),
(7, 7, 'shop_order_status', '', 0, 0),
(8, 8, 'shop_order_status', '', 0, 0),
(9, 9, 'shop_order_status', '', 0, 0),
(10, 10, 'shop_order_status', '', 0, 0),
(11, 11, 'shop_order_status', '', 0, 0),
(12, 12, 'shop_order_status', '', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_usermeta`
--

CREATE TABLE IF NOT EXISTS `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Extraindo dados da tabela `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(11, 1, 'wp_user_level', '10'),
(12, 1, 'dismissed_wp_pointers', 'wp350_media,wp360_revisions,wp360_locks,wp390_widgets,wp330_toolbar'),
(13, 1, 'show_welcome_panel', '1'),
(14, 1, 'session_tokens', 'a:1:{s:64:"5d96ac0314a7778ecbd37b775c217026ea2583477089f18266aa30d8dd26be4b";i:1422035491;}'),
(15, 1, 'wp_dashboard_quick_press_last_post_id', '102'),
(16, 1, 'wp_user-settings', 'libraryContent=upload'),
(17, 1, 'wp_user-settings-time', '1421872370'),
(18, 1, '_woocommerce_persistent_cart', 'a:1:{s:4:"cart";a:1:{s:32:"3def184ad8f4755ff269862ea77393dd";a:8:{s:10:"product_id";i:125;s:12:"variation_id";s:0:"";s:9:"variation";s:0:"";s:8:"quantity";i:1;s:10:"line_total";d:30;s:8:"line_tax";i:0;s:13:"line_subtotal";i:30;s:17:"line_subtotal_tax";i:0;}}}'),
(19, 1, 'billing_country', 'BR'),
(20, 1, 'billing_first_name', 'Jeffersson'),
(21, 1, 'billing_last_name', 'Araujo'),
(22, 1, 'billing_company', ''),
(23, 1, 'billing_address_1', 'asdasd'),
(24, 1, 'billing_address_2', ''),
(25, 1, 'billing_city', 'Macaíba'),
(26, 1, 'billing_state', 'RN'),
(27, 1, 'billing_postcode', '59280000'),
(28, 1, 'billing_email', 'jefferssonrenascer@gmail.com'),
(29, 1, 'billing_phone', '8432711732');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_users`
--

CREATE TABLE IF NOT EXISTS `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(64) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BjaAtJdmQtKTM3rGh7BPiOGwKovxdv1', 'admin', 'jeffersson.araujo@dndtec.com.br', '', '2015-01-09 17:51:15', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) NOT NULL,
  `attribute_label` longtext,
  `attribute_type` varchar(200) NOT NULL,
  `attribute_orderby` varchar(200) NOT NULL,
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `download_id` varchar(32) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL DEFAULT '0',
  `order_key` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `downloads_remaining` varchar(9) DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`,`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_order_itemmeta`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(9, 2, '_qty', '1'),
(10, 2, '_tax_class', ''),
(11, 2, '_product_id', '128'),
(12, 2, '_variation_id', ''),
(13, 2, '_line_subtotal', '100'),
(14, 2, '_line_total', '100'),
(15, 2, '_line_tax', '0'),
(16, 2, '_line_subtotal_tax', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_order_items`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_item_name` longtext NOT NULL,
  `order_item_type` varchar(200) NOT NULL DEFAULT '',
  `order_id` bigint(20) NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(2, '<!--:pt-->Remada na lua<!--:-->', 'line_item', 129);

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_tax_rates`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) NOT NULL DEFAULT '',
  `tax_rate` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) NOT NULL,
  `tax_rate_class` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`),
  KEY `tax_rate_state` (`tax_rate_state`),
  KEY `tax_rate_class` (`tax_rate_class`),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `location_code` varchar(255) NOT NULL,
  `tax_rate_id` bigint(20) NOT NULL,
  `location_type` varchar(40) NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type` (`location_type`),
  KEY `location_type_code` (`location_type`,`location_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `wp_woocommerce_termmeta`
--

CREATE TABLE IF NOT EXISTS `wp_woocommerce_termmeta` (
  `meta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `woocommerce_term_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext,
  PRIMARY KEY (`meta_id`),
  KEY `woocommerce_term_id` (`woocommerce_term_id`),
  KEY `meta_key` (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
