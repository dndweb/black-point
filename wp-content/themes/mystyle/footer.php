<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
	<!-- === Slide 6 / Contact === -->
	<div class="slide story" id="slide-6" data-slide="6">
		<div class="container">
			<div class="copia">PROSUP 2014 © Todos os direitos reservados</div>
			<div class="dnd"><a href="http://dndtec.com.br/"><img src="<?=get_template_directory_uri()?>/images/dnd.png" height="53" width="235"></a></div>
		</div><!-- /container -->
	</div><!-- /Slide 6 -->

</body>

	<!-- SCRIPTS -->
	<script src="<?=get_template_directory_uri()?>/js/html5shiv.js"></script>
	<script src="<?=get_template_directory_uri()?>/js/jquery-1.10.2.min.js"></script>
	<script src="<?=get_template_directory_uri()?>/js/jquery-migrate-1.2.1.min.js"></script>
	<script src="<?=get_template_directory_uri()?>/js/bootstrap.min.js"></script>
	<script src="<?=get_template_directory_uri()?>/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="<?=get_template_directory_uri()?>/fancybox/jquery.fancybox.pack-v=2.1.5.js"></script>
	<script src="<?=get_template_directory_uri()?>/js/script.js"></script>

	<script type="text/javascript" src="<?=get_template_directory_uri()?>/js/cycle.js"></script>

	<!-- fancybox init -->
	<script>
		$(document).ready(function(e) {

			$('.slideIndex').cycle({
			    fx:     'scrollHorz',
			    speed:   1000,
			    timeout: 6000,
                        next:   '.next',
                        prev:   '.prev'
			});

			$(".fancybox").fancybox({
				padding: 10,
				helpers: {
					overlay: {
						locked: false
					}
				}
			});

			$('a').click(function(){
				if($(this).attr("class") != "fancybox" && $(this).attr("class") != "agendar" && $(this).attr("class") != "agendar_2"){
					window.location.assign($(this).attr("href"));
				}
			})

            $("#submitContato").click(function(){

                var nome      = $("#nome").val();
                var email     = $("#email").val();
                var telefone  = $("#telefone").val();
                var mensagem  = $("#mensagem").val();

                // alert("Teste");

                $(".erroContato").html("<img src='<?=get_template_directory_uri()?>/images/loading.gif' alt='Enviando' style='width: 20px' />");

                $.post('<?=get_template_directory_uri()?>/enviaContato.php', {nome: nome, email: email, telefone: telefone, mensagem: mensagem}, function(resposta) {
                 $(".erroContato").slideDown();
                 if (resposta != false) {
                     $(".erroContato").css("color","red");
                     $(".erroContato").html(resposta);
                 }
                 else {
                     $(".erroContato").css("color","green");
                     $(".erroContato").html("Mensagem enviada com sucesso!");
                        document.getElementById("formContato").reset();
                 }
                });
                return false;
            });

		});

    </script>


</html>