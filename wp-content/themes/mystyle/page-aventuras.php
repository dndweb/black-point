<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
			<?php
				$args = array( 'post_type' => 'aventuras');
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			?>
				<div class="col-sm-12 divAventuras">
					<h3><?=the_title()?></h3>
					<div class="descAventura"><?=the_content()?></div>
					<div class="imgAventura" style="background-image: url('<?=$image[0]?>')"></div>
				</div>
			<?php
				endwhile;
			?>
		</div>
	</div>
<?php get_footer(); ?>