<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>


	<!-- === Slide 2 === -->
	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container sobrePag">
			<!-- <h1><?=the_title()?></h1><div class="clear"></div> -->
			<div class="col-sm-8 sobre">
				<h3>Sobre nós</h3>
			<?php
				$args = array( 'post_type' => 'sobre', 'posts_per_page' => 1 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					the_content();
				endwhile;
			?>
			</div>
			<div class="col-sm-4" style="margin-top: 120px">
				<img src="<?=get_template_directory_uri()?>/images/cavalo.png" height="269" width="273">
			</div>
			<div class="col-sm-12 nossoDiferencial">
			<?php
				$args = array( 'post_type' => 'diferencial', 'posts_per_page' => 1 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
			?>
					<h3><?=the_title()?></h3>
			<?php
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					the_content();
				endwhile;
			?>
			</div>
			<div class="col-sm-12 divServicos">
				Faça <a href="<?=$url_base?>/agendamento">aqui</a> seu agendamento, ou clique <a href="<?=$url_base?>/contato">aqui</a> para entrar em contato.
			</div>
		</div><!-- /container -->
	</div><!-- /slide2 -->
<?php get_footer(); ?>