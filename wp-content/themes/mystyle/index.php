<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div class="contSlide">
		<div class="slideIndex">
	<?php
		$args = array( 'post_type' => 'slides', 'posts_per_page' => 5 );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	?>
			<div class="slide story slide-1" style="background-image: url('<?=$image[0]?>')"></div>
	<?php
		endwhile;
	?>
		</div>
		<img class="seta prev" src="<?=get_template_directory_uri()?>/images/prev.png" height="61" width="61">
		<img class="seta next" src="<?=get_template_directory_uri()?>/images/next.png" height="61" width="61">
	</div>
	<div class="slide story" id="slide-2" data-slide="2" style="padding: 10px 0">
		<div class="container">
			<div class="contSocial">
			<?php
				$args = array( 'post_type' => 'redes', 'posts_per_page' => 1 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
			?>
				<a href="<?=get_field('facebook')?>"><div class="col-sm-4"><img src="<?=get_template_directory_uri()?>/images/face.png" style="width: 100%"/></div></a>
				<a href="<?=get_field('instagram')?>"><div class="col-sm-4"><img src="<?=get_template_directory_uri()?>/images/insta.png" style="width: 100%"/></div></a>
				<a href="<?=get_field('youtube')?>"><div class="col-sm-4"><img src="<?=get_template_directory_uri()?>/images/you.png" style="width: 100%"/></div></a>
			<?php
				endwhile;
			?>
			</div>
		</div>
	</div>
	<!-- === Slide 2 === -->
	<div class="slide story" id="slide-2" data-slide="2" style="padding: 20px 0">
		<div class="container">
			<div class="col-sm-4 sobre">
				<h3>Sobre nós</h3>
			<?php
				$args = array( 'post_type' => 'sobre', 'posts_per_page' => 1 );
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					$texto = get_the_excerpt();
					echo $texto;
				endwhile;
			?>
			<a href="<?=$url_base?>/sobre/">Saiba Mais</a>
			</div>
			<div class="col-sm-4">
				<img src="<?=get_template_directory_uri()?>/images/cavalo.png" width="100%">
			</div>
			<div class="col-sm-4 nossoDiferencial">
				<h3>Acesse nosso canal no youtube e confira nossos videos</h3>
				<div class="video">
			<?php
				$args = array( 'post_type' => 'video', 'posts_per_page' => 1);
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
			?>
					<?=the_content()?>
			<?php
				endwhile;
			?>
				</div>
				</div>
			</div>
		</div><!-- /container -->
	</div><!-- /slide2 -->

	<!-- === SLide 3 - Portfolio -->
	<div class="slide story" id="slide-3" data-slide="3">
		<div class="row">
	<?php
		$args = array( 'post_type' => 'galeria', 'posts_per_page' => 8 );
		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) : $loop->the_post();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	?>
			<div class="col-12 col-sm-6 col-lg-2"><a data-fancybox-group="portfolio" href="<?=get_permalink()?>"><img class="thumb" src="<?=$image[0]?>" alt=""></a></div>
	<?php
		endwhile;
	?>
		</div><!-- /row -->
	</div><!-- /slide3 -->

	<!-- === Slide 4 - Process === -->
	<div class="slide story" id="slide-4" data-slide="4">
		<div class="container nossosPrecos">
			<h2>Nossos Preços</h2>
<?php
	$args = array('post_type' => 'product');
	$loop = new WP_Query( $args );
	$cont = 0;
	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) : $loop->the_post();
			global $woocommerce, $product;
			$average = $product->get_average_rating();
?>
			<div class="col-sm-4">
				<div class="pacote">
					<p><?=$product->get_price_html()?></p>
					<a href="<?php the_permalink(); ?>"><div class="preco"><?=the_title()?></div></a>
				</div>
			</div>
<?php
		endwhile;
	} else {
		echo __( 'No products found' );
	}
	wp_reset_postdata();
?>
		</div><!-- /container -->
	</div><!-- /slide4 -->

	<!-- === Slide 5 === -->
	<div class="slide story" id="slide-5" data-slide="5">
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d992.1998024699124!2d-35.167748!3d-5.883816!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2f92979f8a043%3A0xe32245dc76bbe170!2sAv.+Erivan+Fran%C3%A7a%2C+2170+-+Ponta+Negra%2C+Natal+-+RN%2C+59090-100!5e0!3m2!1spt-PT!2sbr!4v1420835357586" width="100%" height="600" frameborder="0" style="border:0"></iframe>
		<div class="enderecoCont">
			<div class="endereco">
				<h1>ONDE NOS ENCONTRAR</h1>
				<ul>
					<li><img src="<?=get_template_directory_uri()?>/images/mapa.png" height="18" width="12"> Av. Erivan França, 2170 - Ponta Negra. Natal - RN. 59090-100</li>
					<li><img src="<?=get_template_directory_uri()?>/images/telefone.png" height="15" width="15"> Fone: +55 84 3012 2929</li>
					<li><span style="margin-left: 59px">+55 84 9648 4005</span></li>
					<li><span style="margin-left: 59px">+55 84 9637 9471</span></li>
					<li><img src="<?=get_template_directory_uri()?>/images/carta.png" height="13" width="18"> Email: contato@prosup.com.br</li>
				</ul>
				<a href=""><img src="<?=get_template_directory_uri()?>/images/face.png" height="41" width="41"></a>
				<a href=""><img src="<?=get_template_directory_uri()?>/images/insta.png" height="41" width="41"></a>
				<a href=""><img src="<?=get_template_directory_uri()?>/images/twitter.png" height="41" width="41"></a>
			</div>
		</div>
	</div><!-- /slide5 -->
<?php get_footer(); ?>