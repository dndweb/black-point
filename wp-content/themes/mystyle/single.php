<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div class="slide story" id="slide-2" data-slide="2">
		<div class="container">
<?php
	while ( have_posts() ) :
		the_post();
		$tipo_post = get_post_type($wp_query->post->ID);
		if($tipo_post == "galeria") :
			$imagem = get_field('imagens');
?>
				<div class="col-sm-12 galeriaInt">
					<h1><?=the_title()?></h1>
					<p><?=the_content()?></p>
	<?php
			if( $imagem ):
				foreach( $imagem as $image ):
	?>
					<a class="fancybox" rel="gallery1"  href="<?=$image['url']?>"><div style="background-image: url( '<?=$image['url']?>' );" class=" col-sm-3 imagemGaleriaEvento" /></div></a>
	<?php
				endforeach;
			endif;
	?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
<?php
		else :
			get_template_part( 'content', get_post_format() );
			twentythirteen_post_nav();
			comments_template();
		endif;
	endwhile;
?>

		</div>
	</div>
<?php get_footer(); ?>