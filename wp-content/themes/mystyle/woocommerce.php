<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
global $woo_options;
get_header(); ?>
	<div class="woo">
		<div class="container" class="<?php echo $lili ?>">
			<div class="container">
				<div id="primary" class="content-area">
					<div id="content" class="site-content" role="main">
						<div class="menuLoja">
							<ul>
								<li><a href="<?=home_url()?>/minha-conta"><div id="minhaConta"></div></a></li>
								<li><div id="carrinho"><a class="cart-contents carinhotopo" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('%d item', '%d items', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></a></div></li>
							</ul>
						</div>
						<div class="clear"></div>
						<div class="row">
							<div class="woocommerce" style="width: 980px; margin: 0 auto;">
								<?php woocommerce_content(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>