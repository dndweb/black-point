<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	<!-- === Slide 5 === -->
	<div class="slide story" id="slide-5" data-slide="5" style="padding-top: 100px">
		<div class="enderecoInterna">
			<div class="container">
				<div class="col-sm-12 contato">
					<h3>Contato</h3>
		 			<form role="form" id="formContato">
						<div class="form-group">
							<input type="email" class="form-control" id="nome" title="Nome" placeholder="Nome">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="email" title="E-mail" placeholder="E-mail">
						</div>
						<div class="form-group">
							<input type="text" class="form-control" id="telefone" title="Telefone" placeholder="Telefone">
						</div>
						<div class="form-group">
							<textarea class="form-control" id="mensagem" rows="3"  title="Mensagem" placeholder="Mensagem"></textarea>
						</div>
						<button type="submit" id="submitContato" class="btn btn-theme btLeft">Enviar</button> <div class="erroContato">Teste</div>
					</form>
				</div>
				<div class="col-sm-6">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d992.1998024699124!2d-35.167748!3d-5.883816!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7b2f92979f8a043%3A0xe32245dc76bbe170!2sAv.+Erivan+Fran%C3%A7a%2C+2170+-+Ponta+Negra%2C+Natal+-+RN%2C+59090-100!5e0!3m2!1spt-PT!2sbr!4v1420835357586" width="95%" height="250" frameborder="0" style="border:0"></iframe>
				</div>
				<div class="col-sm-6 dadosContato">
					<ul>
						<li><img src="<?=get_template_directory_uri()?>/images/mapa.png" height="18" width="12"> Av. Erivan França, 2170 - Ponta Negra. Natal - RN. 59090-100</li>
						<li><img src="<?=get_template_directory_uri()?>/images/telefone.png" height="15" width="15"> Fone: +55 84 3012 2929</li>
						<li><span style="margin-left: 59px">+55 84 9648 4005</span></li>
						<li><span style="margin-left: 59px">+55 84 9637 9471</span></li>
						<li><img src="<?=get_template_directory_uri()?>/images/carta.png" height="13" width="18"> Email: contato@prosup.com.br</li>
					</ul>
					<a href=""><img src="<?=get_template_directory_uri()?>/images/face.png" height="41" width="41"></a>
					<a href=""><img src="<?=get_template_directory_uri()?>/images/insta.png" height="41" width="41"></a>
					<a href=""><img src="<?=get_template_directory_uri()?>/images/twitter.png" height="41" width="41"></a>
				</div>
			</div><!-- /container -->
		</div>
	</div><!-- /slide5 -->
<?php get_footer(); ?>