<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
  <!-- === Slide 2 === -->
  <div class="slide story" id="slide-2" data-slide="2">
    <div class="container sobrePag">
      <h1>Prosup Club</h1>
      <?php
        $args = array( 'post_type' => 'club', 'posts_per_page' => 1);
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
      ?>
          <div class="col-sm-12" style="color: #000000; font-size: 1.2em; text-align: justify"><?=the_content()?></div>
      <?php
        endwhile;
      ?>
    </div><!-- /container -->
  </div><!-- /slide2 -->
<?php get_footer(); ?>