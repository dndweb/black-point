<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
	global $url_base;
	$url_base = get_home_url();
	if($_SERVER['REQUEST_URI'] != "/" ) {
		$lingua = explode($_SERVER['REQUEST_URI'], $_SERVER['REDIRECT_URL']);
		$lingua = $lingua[0];
	} else {
		$lingua = $_SERVER['REDIRECT_URL'];
	}
	$url_base .= $lingua;
	global $woo_options;

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="BlackTie.co - Free Handsome Bootstrap Themes" />
	<meta name="keywords" content="themes, bootstrap, free, templates, bootstrap 3, freebie,">
	<meta property="og:title" content="">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=get_template_directory_uri()?>/fancybox/jquery.fancybox-v=2.1.5.css" type="text/css" media="screen">
	<link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/font-awesome.min.css" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri()?>/css/style.css">

	<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,300,200&subset=latin,latin-ext' rel='stylesheet' type='text/css'>


	<link rel="prefetch" href="<?=get_template_directory_uri()?>/images/zoom.png">
	<?php wp_head(); ?>
</head>

<body>
	<div class="navbar navbar-fixed-top" data-activeslide="1">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>


		<div class="nav-collapse collapse navbar-responsive-collapse">
			<a href="<?=$url_base?>"><img src="<?=get_template_directory_uri()?>/images/logo.png" class="logo"></a>
			<script id="setmore_script" type="text/javascript" src="https://my.setmore.com/js/iframe/setmore_iframe.js"></script>
			<div class="menu">
				<ul>
					<li><a href="<?=$url_base?>">HOME</a></li>
			<?php
				$args = array( 'post_type' => 'menu');
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post();
			?>
					<li><a href="<?=the_field('link')?>"><?=the_title()?></a></li>
			<?php
				endwhile;
			?>
				</ul>
			</div>
			<div class="clear"></div>
			<div id="menu">
				<ul>
					<li><a href="<?=home_url()?>">POR<!--img src="<?=get_template_directory_uri()?>/images/brasil.png" height="18" width="27" / --></a></li>
					<li><a href="<?=home_url()?>/en">ENG<!--img src="<?=get_template_directory_uri()?>/images/usa.png" height="18" width="27" /--></a></li>
					<li><a href="<?=home_url()?>/es">ESP<!--img src="<?=get_template_directory_uri()?>/images/espanha.png" height="18" width="27"/--></a></li>
				</ul>
			</div>
			<div class="row"><div class="col-sm-2 active-menu"></div></div>
		</div>

	</div>